# Notes on syntaxis for setting the config file

I am using `ConfigArgParse` package,
which merges capabilities of `Argparse` and `ConfigParser` from the standar python libraries.

The `ConfigParser` setting in use is `configargparse.ConfigparserConfigFileParser` which uses `python-ini` but with a little twick we can read dictionaries.

## to declare ENV variables

lets we have an environment variable `ENV_EX`,
and we want to be able to change it from the `config.ini` file or from command line. What you need to do is:

1. in the `config.ini` file:
    [ENV VARS]
    env_ex='SOMETHING'
2. in the parser:
    parser.add('--env_ex',env_var="ENV_EX")
3. now you can define it either at the config file, as a regular ENV var or through command line like this:
    1. ENV_EX=SOMETHING ./script.py --config_file 'config.ini'

So basically:
- the parser argument should be lower-case.
- the parser argument name should be the same as the key defined in the config file.
- the name of the env_var should be in between quotes when passing the argument to the parser.

## to declare dictionaries

lets consider the following config.ini example:
[Dict]
test_dict={
    'a':1,
    'b':2,
    }

To read this you need to set the parser like:
parser.add_argument(--test_dict,type=yaml.safe_load)

again Note that the parser type needs to be `configargparse.ConfigparserConfigFileParser` 

## NOTE on PATHS

- absolute paths are prefered!, otherwise they depend of the relative location of the executable, and that might cause bugs.
- in the `config.ini` set them as:
    - path= my_path/
so basically without quotes
