# Documentation on DataProcessing

### WIP
We are in the transition of depricating the use of `RootCore` from the package,
since its a deprecated framework with no more active development and is not easy to accomodate to a Phyton package;
it also implied extra setups for little benefits.

Below I show the previous Data Flow:
```mermaid
graph LR;
    I[ZllyAthDerivation output]-->A(GetNTuple/NTuple);
    S[setup RootCore]-->A;

    A-->A1(load root files into TChain);
    A1-->A2(init TTrees for output);
    A1-->A3(apply cluster strategies);
    A3-->A4(normalize clusters);
    A4-->A5[output root];
    
    A5-->B1(NTupleToImage);
    B1-->B2(split and reshape for TF model input);
```

Now the current WIP implementation:
```mermaid
graph TD;
    subgraph common
        I[ZllyAthDerivation output] --> A(NTupleToImage);
        A-->A1(load root files in batches/chunks);
        A1-->D1(check files quality)
        A1-->A2(remove sample cross contamination)
        A2-->D2(check removal cont.);
        A2-->A3(apply removal clus strategy);
        A3-->D3(check removal from strategy);
        A3-->A4(remove crack-region entries)
        A4-->D4(check empty crack-region);
        A4-->A5(split into subsets);
    end 

    subgraph splitting
        A5---B1[Y:labels];
        A5---B2[Z:extra features];
        A5---B3[Xs:clusters]
        B3-->B4(reshape as img)
        B4-->B5(normalize/scale)
        B5-->B6(split and save)-->output
    end
```

## `data_handler.py`

Contains `Data` class to handle the outputs of `NTupleToImage`.
Given a specific *usage* this class will append the Signal and Background data to the same container,
and will create different containers for the differents steps of the training,
being *training*, *validation* and *testing*.

## `NTupleToImage.py`

Input:: `.root` files from `NTuple` output.
These are expected to have 3 different TTrees (Train, Val and Test).

Output:: `.npy` and `.h5` binary files for an easy manipulation of data.

Given a file it loads its TTree content into a `PandasDataFrame` from which it extracts different features.
By the way, all relevant features that want to be loaded should be written in `features.txt`.

- Takes a `features` file, where one specifies what variables you want to read.
- Takes a `sample` file, where one specifies the name of the sample file to read, that are located at `Input`. If many files have a common string in their name, that can be use to load many files at a time, for example if you have: `file_1`, `file_2`, ..., etc you can write `file_*` to load all those files.
- Basically this script takes a root file and loads its data, taking into account the features specified.
With that data creates 4 NumpyArrays: X1, X2, X3 that are the Layer N Calo cells array; and Y the array of labels and other variables (such as pT, SSvars, etc).
- Each array is stored in a binary file with format NPY.

