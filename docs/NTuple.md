# NTuple documentation

AnalysisBase framework to apply strategy (0,1,2 and 3) and signal/background selections,
- Strategy: 0, 1, 2, or 3. \
    0 : Healthy cluster and $|\eta|<1.8$; \
    1 : Healthy cluster and $|\eta|<2.5$; \
    2 : UnHealthy cluster and $|\eta|<2.5$; \
    3 : UnHealthy cluster and $|\eta|<1.8$;

- Selection : GammaJet or DiJet, i.e. only MC data training available.

## Setup

On the first time running:
```shell
cd ../;     # if not outside `NTuple/` this won't run
setupATLAS;
lsetup 'rcsetup Base,2.5.1';
rc find_packages;
rc compile;
```

Every logging:
```shell
source rcSetup.sh
``` 

**NOTE**: since this part of the package uses ATHENA,
it is important to run it in a clean session apart from the rest,
if not some confilcts between ROOT and Python may ocurre.

## runs as :

> `runCleanAndGet Input_Dir Ouput_Dir Strategy Evt_first Evt_last Selection`

## General remarks

- Takes input from [ZllAthDerivation](https://gitlab.cern.ch/mobelfki/zllyathderivation) pkg

- Two samples needed: Background (Bkg) and Signal(Sig)
