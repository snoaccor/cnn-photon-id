# Offline and HLT training
To set the environment for this package you will need to have version of Python3.6+ installed.
Then in a terminal (assuming you're using python37):
> python3.7 -m venv cnn-photon-id/cnn_phID_env


If using bash:
> cd cnn_phID_env
> source ./cnn_phID_env/activate
If using fish:
. ./cnn_phId_env/bin/activate.fish

Update pip inside env:
> python3 -m pip install --upgrade pip
> pip install --upgrade pip

Load requirements:
> python3 -m pip install -r py_requirements
> pip install -r py_requirements.txt 

To deactivate
> deactivate
