#!/usr/bin/env python3
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
sys.path.append('../../')
from DataProcessing.DataProcessing import Data
import itertools
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

#np.set_printoptions(threshold=sys.maxsize)

import os
import math
import ROOT

seed = 215
np.random.seed(seed)

data = Data(0)

Y = data.Y_Test

E_Lr1   = data.X1_Test[:,0,:];
E_Lr2   = data.X2_Test[:,0,:];
E_Lr3   = data.X3_Test[:,0,:];

phiSize = 2
etaSize = 56

data = E_Lr1;

for i in range(0,2000):
    label = ['Bkg','Signal']
    image = []
    sumE = np.sum(data[i,:])
    for phi in range(0,phiSize):
        etaline = []
        for eta in range(0,etaSize):
            k = phi + phiSize*eta
            etaline.append(data[i,k]/sumE)
        image.append(etaline)
    image = np.matrix(image)
    plt.figure()
    plt.imshow(image)
    plt.colorbar()
    plt.title('Photon_'+str(i+1)+'_'+label[Y[i]])
    #plt.show()
    plt.savefig('img/Lr1/Photon_'+str(i+1)+'_'+label[Y[i]]+'.png')
    plt.close()

exit()

print(E.shape)

# Plot the surface.
fig = plt.figure()
ax = fig.gca(projection='3d')
surf = ax.plot_surface(Phi, Eta, E, cmap=cm.coolwarm, linewidth=0, antialiased=False)
#
# Customize the z axis.
ax.set_zlim(-1.0, 1.0)
ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

# Add a color bar which maps values to colors.
fig.colorbar(surf, shrink=0.5, aspect=5)
plt.show()

