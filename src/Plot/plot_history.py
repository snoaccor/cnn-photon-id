#!/usr/bin/env python3
"""
File: plot_history
Author: Santiago Javier Noacco Rosende
Email: snoaccor@cern.ch
Github:
Description: Will plot all the history from your trained model.
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import plot_mod as Mod
import os
import sys
import argparse
import logging

def config_Parse():
    """
    Set all the configuration to your parser object.

    # Args::None

    # Returns::parser object.
    """
    parser = argparse.ArgumentParser('plot')
    parser.add_argument('-I', '--Input', required=True, help='<Input folder or file/s>' )
    parser.add_argument('-M', '--ModelTag', required=True, help='Model tag' )
    parser.add_argument('-O', '--Output', required=True, help='<Output folder or files/s>')
    parser.add_argument('-D','--Debug', required=False, help='Debug flag', action='store_true')
    return parser

def _laod_history_from_CSVLogger(path):
    with open(path,'r') as f:
        historyDF = pd.read_csv(path)
    logging.info(historyDF.columns)
    return historyDF

def plot_and_save(history_DF,OutDir,ext='png'):
    # -- Set epochs
    epochs = history_DF.pop('epoch')
    # -- Set val style
    val_sty = {
            'plot':{
                'marker':'o',
                'mfc':'none',
                'color':'Brown',
                'linestyle':'--'
                }
            }
    figures = {}
    axes = {}

    # -- Get unique metrics names
    metrics=np.unique([idx.split('_')[-1] for idx in history_DF.columns])
    for m in metrics:
        # -- Init figure
        figures[m], axes[m] = plt.subplots()

        Mod.mplot(axes[m], epochs, history_DF[m], 'train')
        Mod.mplot(axes[m], epochs, history_DF[f'val_{m}'], 'val', style_dict=val_sty['plot'])

        axes[m].set_xlabel('epochs')
        axes[m].set_ylabel(m)
        axes[m].legend()

        filename = f'{m}vsEp.{ext}'
        figures[m].savefig(os.path.join(OutDir,filename))


def main(argv):
    parser = config_Parse()
    args = parser.parse_args()

    historyDF = _laod_history_from_CSVLogger(args.Input)
    # history_npy = _load_history_from_npy(args.Input)
    # logging.info(f' --> Your history: {historyDF.keys()}')

    OutDir = f'{args.Output}/{args.ModelTag}/history'
    try:
        logging.info(f'making {OutDir}')
        os.makedirs(OutDir)
    except FileExistsError as e:
        pass
        # logging.error(e)
        # raise(e)

    # plot_and_save(history_npy,OutDir)
    plot_and_save(historyDF,OutDir)
    return

if __name__ == '__main__':
    main(sys.argv[1:])
