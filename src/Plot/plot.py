#!/usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
sys.path.append('../../')
from DataProcessing.DataProcessing import Data
import itertools

#np.set_printoptions(threshold=sys.maxsize)

import os
import math
import ROOT

seed = 215
np.random.seed(seed)

data = Data(0);


def plot_var(true, pred, conv,name):
    lst = ['Inc', 'UnConv', 'Conv']
    sig  = []
    bkg  = []
    if conv == 0:
        for i in range(true.shape[0]):
            if true[i] == 1:
                sig.append(pred[i]*1.);
            else:
                bkg.append(pred[i]*1.);
    if conv == 1:
        for i in range(true.shape[0]):
            if data.Z_Train[i,-4] == 0:
                if true[i] == 1:
                    sig.append(pred[i]*1.);
                else:
                    bkg.append(pred[i]*1.);
    if conv == 2:
        for i in range(true.shape[0]):
            if data.Z_Train[i,-4] != 0:
                if true[i] == 1:
                    sig.append(pred[i]*1.);
                else:
                    bkg.append(pred[i]*1.);

    plt.figure()
    plt.hist(sig, bins=50, range=[np.min(np.array(pred)), np.max(np.array(pred))], histtype='step',  density='True')
    plt.hist(bkg, bins=50, range=[np.min(np.array(pred)), np.max(np.array(pred))], histtype='step',  density='True')
    plt.xlabel('p')
    plt.ylabel('Event')
    plt.legend(['Real photon','Fake photon'])
    plt.savefig(name+"_"+str(lst[conv])+".png")
    plt.close()

def plot_all_var(data):
    cv = 0
    # FIXME: new Z DF style is incompatible with this notation.
    plot_var(data.Y_Train,data.Z_Train[:,0],cv,'Lr1Sum7x11')
    plot_var(data.Y_Train,data.Z_Train[:,1],cv,'Lr2Sum7x11')
    plot_var(data.Y_Train,data.Z_Train[:,2],cv,'Lr3Sum7x11')
    plot_var(data.Y_Train,data.Z_Train[:,3],cv,'PreSSum7x11')
    plot_var(data.Y_Train,data.Z_Train[:,4],cv,'pt')
    plot_var(data.Y_Train,data.Z_Train[:,5],cv,'eta')
    plot_var(data.Y_Train,data.Z_Train[:,6],cv,'phi')
    plot_var(data.Y_Train,data.Z_Train[:,7],cv,'e')
    plot_var(data.Y_Train,data.Z_Train[:,8],cv,'topoetcone20')
    plot_var(data.Y_Train,data.Z_Train[:,9],cv,'topoetcone30')
    plot_var(data.Y_Train,data.Z_Train[:,10],cv,'topoetcone40')
    plot_var(data.Y_Train,data.Z_Train[:,11],cv,'etcone20')
    plot_var(data.Y_Train,data.Z_Train[:,12],cv,'etcone30')
    plot_var(data.Y_Train,data.Z_Train[:,13],cv,'etcone40')
    plot_var(data.Y_Train,data.Z_Train[:,14],cv,'ptvarcone20')
    plot_var(data.Y_Train,data.Z_Train[:,15],cv,'ptvarcone30')
    plot_var(data.Y_Train,data.Z_Train[:,16],cv,'ptvarcone40')
    plot_var(data.Y_Train,data.Z_Train[:,17],cv,'ptcone20')
    plot_var(data.Y_Train,data.Z_Train[:,18],cv,'ptcone30')
    plot_var(data.Y_Train,data.Z_Train[:,19],cv,'ptcone40')
    plot_var(data.Y_Train,data.Z_Train[:,20],cv,'Ethad')
    plot_var(data.Y_Train,data.Z_Train[:,21],cv,'Ethad1')
    plot_var(data.Y_Train,data.Z_Train[:,22],cv,'Rhad')
    plot_var(data.Y_Train,data.Z_Train[:,23],cv,'Rhad1')
    #plot_var(data.Y_Train,data.Z_Train[:,24],0,'E011')
    plot_var(data.Y_Train,data.Z_Train[:,25],cv,'E132')
    plot_var(data.Y_Train,data.Z_Train[:,26],cv,'E237')
    plot_var(data.Y_Train,data.Z_Train[:,27],cv,'E277')
    plot_var(data.Y_Train,data.Z_Train[:,28],cv,'Reta')
    plot_var(data.Y_Train,data.Z_Train[:,29],cv,'Rphi')
    plot_var(data.Y_Train,data.Z_Train[:,30],cv,'Weta1')
    plot_var(data.Y_Train,data.Z_Train[:,31],cv,'Weta2')
    plot_var(data.Y_Train,data.Z_Train[:,32],cv,'f1')
    plot_var(data.Y_Train,data.Z_Train[:,33],cv,'f3')
    plot_var(data.Y_Train,data.Z_Train[:,34],cv,'f3core')
    plot_var(data.Y_Train,data.Z_Train[:,35],cv,'fracs1')
    plot_var(data.Y_Train,data.Z_Train[:,36],cv,'Wstot1')
    plot_var(data.Y_Train,data.Z_Train[:,37],cv,'deltaE')
    plot_var(data.Y_Train,data.Z_Train[:,38],cv,'Eratio')
    plot_var(data.Y_Train,data.Z_Train[:,39],cv,'E2tsts1')
    plot_var(data.Y_Train,data.Z_Train[:,40],cv,'Emins1')
    plot_var(data.Y_Train,data.Z_Train[:,41],cv,'Emaxs1')
    plot_var(data.Y_Train,data.Z_Train[:,42],cv,'mu')
    plot_var(data.Y_Train,data.Z_Train[:,43],cv,'Conversion')

def main():

    plot_all_var(data)


if __name__== '__main__':
	main()

