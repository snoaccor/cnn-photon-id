
#set input 'trainoutput/HLT/stg_1/2020-07-27_64x2CL_32x2DL_0.08dr_30Ep_1.0e-04lr/history/'
#set input 'trainoutput/HLT/stg_1/2020-08-03_64x2CL_32x2DL_0.08dr_15Ep_1.0e-04lr/history/'
#set model '2020-08-03_64x2CL_32x2DL_0.08dr_15Ep_1.0e-04lr'
#set input 'trainoutput/HLT/stg_1/2020-08-05_128x2CL_256x2DL_0.08dr_15Ep_1.0e-04lr/history/'
#set input 'trainoutput/HLT/stg_1/2020-08-06_16x1CL_32x1DL_0.08dr_15Ep_1.0e-04lr/history'
#set model '2020-08-06_16x1CL_32x1DL_0.08dr_15Ep_1.0e-04lr'
#===================================
#set input 'trainoutput/HLT/stg_1/2020-08-12_32x1CL_64x1DL_0.08dr_30Ep_1.0e-04lr/history'
#set input '/home/santi_noacco/Desktop/WorkingFolder/BelfkirCode/remote/pkg_example_at_lxplus/Train/2020-09-09_32x1CL_128x1DL_0.08dr_50Ep_1.0e-04lr/history/'
#set model '2020-09-09_32x1CL_128x  1DL_0.08dr_50Ep_1.0e-04lr'
set input '../NeuralNetworks/Train/output/HLT/stg_1/20201007_16x1CL_32x1DL_0.08dr_50Ep_1.0e-04lr/history/training.log'
set model '20201007_16x1CL_32x1DL_0.08dr_50Ep_1.0e-04lr'

./plot_history.py -I $input -M $model -O output
