#!/usr/bin/env python3
#=============#
# Authors: Mohamed Belfkir, Santiago Noacco Rosende
# Mantainer: Santiago Noacco Rosende
# Contact: snoaccor@cern.ch
# GitLab:https://gitlab.cern.ch/snoaccor
# GitHub:https://github.com/santinoacco
#=============#
"""
This script aims to load all data from a number of files,
get the relevant arrays and save them in a more usefull format for
later trainning.
"""
# import uproot as pr
import pandas as pd
import numpy as np
import numba
import sys
# import uproot4
# import awkward1
import uproot
import h5py
# try:
    # import uproot4
    # import awkward1
# except ModuleNotFoundError:
    # !pip install uproot4 --user;
    # !pip install awkward1 --user;
    # sys.path.append("")

sys.path.append('../../')
# sys.path.append('../')
import os
from src.common.parser import set_parser
from src.common.script import Script
import data_processing_utils as utils


# TODO add logging

class Step(NTupleToImg):
    def __init__(self, number, info, color="DarkBlue", linestyle="-"):
        self.nbr = number
        self.info = info
        self.color = color
        self.ls = linestyle

    def __repr(self):
        return "step {0}: {1}".format(self.nbr,self.info.upper())

class Strategy(NTupleToImg):
    """
    Use like:
        Stg_X = Strategy(nbr,usage,*args)
        filt = Stg_X.apply(data)
    """
    
    def __init__(self, nbr, usage, info = None):
        doc_dict = {
            0:self.Zero.__doc__,
            1:self.One.__doc__
        }
        self.nbr = nbr
        self.usage = usage
        if info:
            self.info = info
        else:
            self.info = doc_dict[self.nbr]
            assert self.info is not None, "Strategy must be explained, please fill a '__doc__' string"
    
    def apply(self, data):
        """
        Returns filter according to a given strategy
        """
        assert isinstance(data,pd.DataFrame), "Expecting `data` as pd.DataFrame"
        if self.nbr == 0: return self.Zero(data)
        elif self.nbr == 1: return self.One(data)
        else: raise NotImplementedError
    
    def Zero(self, data):
        """
        STRICT: 
            returns true when data clusters have the exact expected target
            size from their respective CaloLayer in full Eta range.
        """
        if not self.info:
            self.info = self.Zero.__doc__
        Lr2cond = utils.get_filt_eq_targetsize(data, "ph_clusterCellsLr2E7x11", 77)
        if self.usage.upper() == 'OFFLINE':
            Lr1cond = utils.get_filt_eq_targetsize(data, "ph_clusterCellsLr1E7x11", 122)
            Lr3cond = utils.get_filt_eq_targetsize(data, "ph_clusterCellsLr3E7x11", 44)
            
            print(f"L1cond: VC=\n{Lr1cond.value_counts()}")
            print(f"L2cond: VC=\n{Lr2cond.value_counts()}")
            print(f"L3cond: VC=\n{Lr3cond.value_counts()}")
            
            return Lr1cond & Lr2cond & Lr3cond
        elif self.usage.upper() == 'HLT':
            return Lr2cond
        else: raise NotImplementedError
    
    def One(self, data):
        """
        STRICT-Lr1:
            for usage "HLT" this strategy is the same as Zero,
            for usage "OFFLINE" this strategy keeps the same for Lr3 but it modifies
            for Lr1 where it remains strict on the target shapes,
            but these expected shapes change with the value of Eta.
        """
        if not self.info:
            self.info = self.One.__doc__
        Lr2cond = utils.get_filt_eq_targetsize(data, "ph_clusterCellsLr2E7x11", 77)
        
        if self.usage.upper() == 'OFFLINE':
            Lr3cond = utils.get_filt_eq_targetsize(data, "ph_clusterCellsLr3E7x11", 44)    

            L1low = utils.get_filt_eq_targetsize(data, "ph_clusterCellsLr1E7x11", 122) & utils.get_filt_btw_eta(data, abs_eta=(0,1.8))
            L1mid = utils.get_filt_eq_targetsize(data, "ph_clusterCellsLr1E7x11", 84) & utils.get_filt_btw_eta(data, abs_eta=(1.8,2))
            L1high = utils.get_filt_eq_targetsize(data, "ph_clusterCellsLr1E7x11", 56) & utils.get_filt_btw_eta(data, abs_eta=(2,2.5))

            Lr1cond = L1low | L1mid | L1high
            return Lr1cond & Lr2cond & Lr3cond
        elif self.usage.upper() == "HLT":
            return Lr2cond
        else: raise NotImplementedError


class NTupleToImg(Script):
    # def __init__(self, stage, sample_name, treename):
    def __init__(self, stage, treename):
        # TODO: deprecate treename
        self.treename = treename
        self._valid_strategies = [];
        super().__init__(stage)
        # print(self.stage)
        # print(self.args.dp_in)
        self._get_samples_names(self.args)
        self._load_features(self.args)
        self._set_out_subdirs

    def _load_features(self,args):
        # -- Read features file names
        features_file = args.features_file
        with open(features_file,'r') as features_file:
            self.features = features_file.read().splitlines();
            assert isinstance(self.features,list)

    def _get_samples_names(self,args):
        # -- Read sample file names
        with open(args.samples_file,'r') as samples_file:
            self.sample_name = samples_file.read().splitlines()[0]
            # log.debug(self.sample_name)

    @property
    def _set_out_subdirs(self):
    # def set_outdirs(self):
        # -- get stg name
        stgname = self._getOutName_Stg(self.sample_name)
        # --Create dir outdir/stgname/treename
        self.fulloutdir = f'{self.OutDir}/{stgname}/{self.treename}'
        # log.debug('fulloutdir exists?',os.path.exists(self.fulloutdir))
        # print('fulloutdir exists?',os.path.exists(self.fulloutdir))
        try:
            os.makedirs(self.fulloutdir)
            print(f' --> new dir {self.fulloutdir}')
        except FileExistsError:
            print(f' --> dir {self.fulloutdir} already exists')

    def _getOutName_Stg(self, sample):
        """
        sets strategy name for sample to output
        """
        # TODO make this less specific,
        #      right now it's too related to IsoTight.
        if "Strategy_IsoTight_1" in sample:
            return 'stg_1';
        if "Strategy_IsoTight_2" in sample:
            return 'stg_2';
        if "Strategy_IsoTight_3" in sample:
            return 'stg_3';
        if "Strategy_IsoTight_0" in sample:
            return 'stg_0';

    def _getOutName_Selection(self, sample):
        """
        sets selection name for sample to output.
        It's use for labeling
        """
        if "DiJet" in sample: return 'DiJet_IsoTight';
        if "GammaJet" in sample: return 'GammaJet_IsoTight';

    def define_strategies(self):
        if self.usage == "HLT":
            pass
        elif self.usage == "OFFLINE":
            
            pass
        else
            raise NotImplementedError
        pass


    def set_to_img(self,data_DF,Eta,Phi,Debug=False):
        """
        Transform data_df -> np.Array,
        then it normalize the array by the total sum of the array energy.
        Finally it reshapes the Array into a matrix like form with dims:
        (Eta,Phi,1).
        """
        X = data_DF.to_numpy(copy=True) # we might not need this anymore?
        if Debug: print('X shape before:',X.shape)
        # -- Sum the energy of each array.
        tot_E=np.asarray([x.sum(axis=0) for x in X])
        # tot_E=np.asarray([np.abs(x.sum(axis=0)) for x in X])
        # -- Assert no 0 sum
        Zero_E = np.where(any(tot_E)==0.)
        assert Zero_E is not None, f'Arrays {Zero_E} have a total energy of 0.'

        # -- Normalize
        X/=tot_E
        # -- Reshape
        X_r = np.asarray([opt_reshape(x,Eta,Phi) for x in X])
        # X_r = [opt_reshape(x,Eta,Phi) for x in X]
        # X_r = np.asarray([x.reshape(Eta,Phi,1) for x in X])

        return X_r

    def old_save_data(self,data,data_name,sample,fmt='NPY'):
        selecname = self._getOutName_Selection(sample)
        # --Def helper dict
        if fmt=='NPY':
            ofn = f'{selecname}_{data_name}.npy'
            path = os.path.join(self.fulloutdir,ofn)
            np.save(file=path,arr=data)
        if fmt=='HDF5':
            ofn = f'{selecname}_{data_name}.h5'
            path = os.path.join(self.fulloutdir,ofn)
            data.to_hdf(
                    path,
                    key=f'{data_name}',
                    # format='table',
                    # mode='a'
                    )

    def save_data(self, data, data_name, sample):
        """
        helper method to store data in NPY format into default output.
        Run this method after `execute` otherwise arrays will be empty.

        # Parameters:

            sample: the desired sample

        # Return: None
        """
        # TODO: check if saving everything in h5 has any inconvinient,
        #       because it might be useful to have everything in the same format to run at the grid.

        selecname = self._getOutName_Selection(sample)
        ofn = f'{selecname}_{data_name}.h5'
        path = os.path.join(self.fulloutdir,ofn)
        if isinstance(data,pd.DataFrame):
            data.to_hdf(
                    path,
                    key=f'{data_name}',
                    format='table',
                    mode='a'
                    )

        if isinstance(data,np.ndarray):
            with h5py.File(path,'a') as f:
                f.create_dataset(
                        data_name,
                        data=data,
                        shape=data.shape,
                        dtype='i',
                        )

    def execute_lazy(self,data_frame,sample,chunk_num):
        """
        """
        X2_df = data_frame.pop('clusterCellsLr2E7x11')
        X1_df = data_frame.pop('clusterCellsLr1E7x11')
        X3_df = data_frame.pop('clusterCellsLr3E7x11')
        # -- defines labels for MC
        Y_df = data_frame.pop('isTruthMatch')
        # -- keep all other relevant vars in Z.
        Z_df = data_frame
        # -- Save non-training variables
        self.old_save_data(
                data=Z_df,
                data_name=f'Z_{chunk_num}',
                sample=sample,
                fmt='HDF5'
                )
        # self.save_data(
                # data=Z_df,
                # data_name=f'Z_{chunk_num}',
                # sample=sample,
                # )
        del Z_df
        # -- Save labels
        self.old_save_data(
                data=Y_df,
                data_name=f'Y_{chunk_num}',
                sample=sample,
                )
        # self.save_data(
                # data=Y_df,
                # data_name=f'Y_{chunk_num}',
                # sample=sample,
                # )
        del Y_df
        # -- Set and Save X1
        X1 = self.set_to_img(
                data_DF=X1_df,
                Eta=self.args.calo['Lr1']['num_eta_cells'],
                Phi=self.args.calo['Lr1']['num_phi_cells'],
                )
        del X1_df
        # print('---- X1')
        # print(type(X1), X1.shape)
        self.old_save_data(
                data=X1,
                data_name=f'Lr1Img_{chunk_num}',
                sample=sample,
                )
        # self.save_data(
                # data=X1,
                # data_name=f'Lr1Img_{chunk_num}',
                # sample=sample,
                # )

        # -- Set and Save X2
        X2 = self.set_to_img(
                data_DF=X2_df,
                Eta=self.args.calo['Lr2']['num_eta_cells'],
                Phi=self.args.calo['Lr2']['num_phi_cells'],
                )
        del X2_df
        # print('---- X2')
        self.old_save_data(
                data=X2,
                data_name=f'Lr2Img_{chunk_num}',
                sample=sample,
                fmt='NPY'
                )
        # self.save_data(
                # data=X2,
                # data_name=f'Lr2Img_{chunk_num}',
                # sample=sample,
                # )

        # -- Set and Save X3
        X3 = self.set_to_img(
                data_DF=X3_df,
                Eta=self.args.calo['Lr3']['num_eta_cells'],
                Phi=self.args.calo['Lr3']['num_phi_cells'],
                )
        del X3_df
        # print('---- X3')
        self.old_save_data(
                data=X3,
                data_name=f'Lr3Img_{chunk_num}',
                sample=sample,
                )
        # self.save_data(
                # data=X3,
                # data_name=f'Lr3Img_{chunk_num}',
                # sample=sample,
                # )

    def loadTree_lazy(self, filepath=None, num_chunks=1):
        """
        For every file in "filepath",
        this will load the data of the TTree,
        for the specified features in "features.txt".
        It will save it in chunks.

        # Args:
            filepath: path to locate .root files
            num_chunks: total number of files to read. Default is 1
        """
        if filepath is None:
            filepath = f'{self.InDir}/{self.sample_name}'
        # log.debug(filepath)
        # -- Set an index iter.
        idx=0;
        # -- Get chunk_size for the corresponding Tree
        chunk_size = round(len(uproot4.lazy(f'{filepath}:{self.treename}'))/num_chunks)
        for arrays in uproot4.iterate(
                files=f'{filepath}:{self.treename}',
                filter_name=self.features,
                step_size=chunk_size,
                library='np',   # we can do this because we know there won't be issues
                ):
            # -- NOTE: we use data frames because "set_to_img" expects them.
            df = pd.DataFrame(data=arrays)
            # -- Save in chunks and in the correct format
            self.execute_lazy(
                    data_frame=df,
                    sample=self.sample_name,
                    chunk_num=idx
                    )
            idx+=1

    def get_subsets(self, data, as_clusters, as_signal_label):
        """
        """
        # -- use 'as_signal_label' feature to set Labels
        Y = data.pop(as_signal_label)
        # --
        _posible_valid_layers = ['1','2','3']
        Xs = {}
        for name in as_clusters:
            for lrnbr in _posible_valid_layers:
                if f"Lr{lrnbr}" in name:
                    Xs[f"X{lrnbr}"] = data.pop(name)
        # -- save remaining scalar features in data frame
        Z = data
        return Z, Y, Xs

    def save_data(data,path, data_name, sample):
        assert sample.lower() in ['sig','bkg'], "sample should be 'sig' or 'bkg'"
        # Sig_Lr1Img.h5
        ofn = "{0}_{1}.h5".format(sample,data_name)
        path = "/".join([path,ofn])
        if isinstance(data, pd.Series) or isinstance(data, pd.DataFrame):
            data.to_hdf(
                path,
                key=data_name,
    #             format="table",
    #             mode='a'
            )
        elif isinstance(data, np.ndarray):
            np.save(file=path.replace(".h5",".npy"),
                    arr=data
                   )

    def split_and_save_subset(data,split,path,data_name,chk,sample):
        assert isinstance(data,pd.Series) or isinstance(data, pd.DataFrame), \
            "data shape expected is pd.Series or pd.DataFrame"
        N = data.shape[0]

        Ntrain = int(N*split[0])
        Nval = int(N*split[1])
        Ntest = int(N*split[2])

        dataTrain = data[:Ntrain] # first Ntrain entries
        dataVal = data[Ntrain:Ntrain+Nval] # from Ntrain to Ntrain+Ntest entries
        dataTest = data[Ntrain+Nval:Ntrain+Nval+Ntest]
        
        
        save_data(dataTrain, path=path,
                  data_name=f'{data_name}train_{chk}',
                  sample=sample)
        save_data(dataVal, path=path,
                  data_name=f'{data_name}val_{chk}',
                  sample=sample)
        save_data(dataTest, path=path,
                  data_name=f'{data_name}test_{chk}',
                  sample=sample)

    def execute(self, data, chk, as_clusters, as_signal_label, steps='all', do_DSQ=True):
        """
        Configures the execution
        """

        # -- Step 0 is loading and checking the Status
        S0 = Step(0,"loaded",color="LightBlue")
        _ = compute_DSQ(data, step=S0,
                        title="test_chk{0}".format(chk),
                        outdir='./',    # TODO: change accordingly
                        as_clusters=as_clusters)

        fig_DSQ, ax_DSQ = plt.subplost(1,len(as_clusters))

        # -- STEP 1: remove contamination in samples
        S1 = Step(1,info='rm contamination in sample')
        # print("--> Removing contamination in sample")
        data = remove_label_contamination(filepath,data)
        # TODO: log this
#         print(data.columns)
#         print(data["ph_isTruthMatch"].value_counts())
        ax_DSQ = compute_DSQ(data, ax=ax_DSQ, step=S1,
                             title="test_chk{0}".format(chk),
                             outdir='./',
                             as_clusters=as_clusters)
        # TODO: log this
        print("--> DONE: Removing contamination in sample")
        
        # -- STEP 2: apply cluster removal strategy
        S2 = Step(2,info='apply clus stg',linestyle='-')
        print("--> Removing cluster according to strategy")
        before_cnt = data["ph_clusterCellsLr2E7x11"].shape[0]
        Stg = Strategy(1,'offline')
        filt = Stg.apply(data)
        # CAREFUL THIS IS REMOVING ELEMENTS AND PLACING IN SAME DF.
        data = data.where(filt).dropna()
        after_cnt = data["ph_clusterCellsLr2E7x11"].shape[0]
        ax_DSQ = compute_DSQ(data, ax=ax_DSQ, step=S2, 
                             title="test_chk{0}".format(chk),
                             outdir='./',
                             as_clusters=as_clusters)
        print("--> DONE: Removing cluster according to strategy, {0} total entries removed".format(before_cnt - after_cnt))
 
        # -- STEP 3
        S3 = Step(3,info='rm crack-reg',color="LightGreen",linestyle="--")
        print("--> Removing cluster in crack region")
        before_cnt = data["ph_clusterCellsLr2E7x11"].shape[0]
        below_crack_region = get_filt_btw_eta(data, abs_eta=(0,1.37))
        over_crack_region = get_filt_btw_eta(data, abs_eta=(1.52,2.5))
        not_in_crack = below_crack_region | over_crack_region
        data = data.where(not_in_crack).dropna()
        after_cnt = data["ph_clusterCellsLr2E7x11"].shape[0]
        ax_DSQ = compute_DSQ(data, ax=ax_DSQ, step=S3,
                             title="test_chk{0}".format(idx),
                             outdir='./',
                             as_clusters=as_clusters)
        print("--> DONE: Removing cluster in crack region, {0} total entries removed".format(before_cnt - after_cnt))
        
        # -- STEP 4: unpacking
        S4 = Step(4,info="unpacking subsets",color="DarkGreen",linestyle="--")
        Z, Y, Xs = get_subsets(data, as_clusters, as_signal_label)

        # -- STEP 5: reshape Xs 
        S5 = Step(5,info="reshape clusters",color="Orange",linestyle="--")
        Xs_reshaped = {xname:do_reshape(Xs[xname]) for xname in Xs.keys()}

        # -- SETP 6: normalization
        S6 = Step(6,info="applying normalization", color="DarkOrange", linestyle=":")
        Xs_norm = {xname:NormalizeClusters(Xs_reshaped[xname], norm_type=1, use_min_offset=True) for xname in Xs_reshaped.keys()}

        # -- STEP 7: split into subsets and save
        S7 = Step(7,info="split and save", color="Purple")
        split_and_save_subset(Y,split=DS_split, path=self.outdir,data_name="Y", chk=chk, sample="sig")
        split_and_save_subset(Z,split=DS_split, path=self.outdir,data_name="Z", chk=chk, sample="sig")
        for xname in Xs_norm.keys():
            split_and_save_subset(Xs_norm[xname], split=DS_split, path=self.outdir, data_name=xname, chk=chk, sample="sig")

        
    def load_data_lazy(self, filepath, num_chunks, features, DS_split=(.5,.25,.25), as_clusters, as_signal_label):
        """
        For every file in "filepath",
        this will load the data,
        for the specified features in "features.txt".
        It will save it in chunks.
        """
        # -- Set an index iter.
        idx=0;
        # -- Get chunk_size for the corresponding Tree
    #     chunk_size = round(len(uproot4.lazy(f'{filepath}:{self.treename}'))/num_chunks)
        chunk_size = round(len(uproot.lazy(f'{filepath}:NTUP'))/num_chunks)
        print(chunk_size)
        for arrays in uproot.iterate(
                files=f'{filepath}:NTUP',
                filter_name=features,
                step_size=chunk_size,
                library='np',   # we can do this because we know there won't be issues
                ):
            # -- NOTE: we use data frames because "set_to_img" expects them.
            df = pd.DataFrame(data=arrays)
            # -- TODO: test Spark instead of Pandas?
            
            # --  adress DataSetQuality on each chunk of data.
            as_clusters = ["ph_clusterCellsLr1E7x11",
                           "ph_clusterCellsLr2E7x11",
                           "ph_clusterCellsLr3E7x11"]
            as_signal_label = 'ph_isTruthMatch'

            self.execute(self, data=df, as_clusters, as_signal_label) 

    



def main():
    """
    The main function of NTupleToNumpy
    """

    trees = ('Tree_Train','Tree_Val','Tree_Test')

    for tree in trees:
        print(f'--> Reading TTree: {tree}')
        NTuple2Img = NTupleToImg(
                stage='PREPROCESS',
                treename=tree,
                );
        NTuple2Img.loadTree_lazy(
                num_chunks=NTuple2Img.args.num_chunks,
                )
        # -- Free some memory, quite important step!
        del NTuple2Img

if __name__ == '__main__':
    main()

# class NTupleToImg(object):

    # def __init__(self, sample_name, outdir, treename):
        # self.args = set_parser()
        # self.sample_name = sample_name
        # self.outdir = outdir
        # self.treename = treename
        # self._load_features(self.args)
        # # -- Build dirs.
        # self.set_outdirs()

# def main():
    # args=set_parser()
    # # -- Set default output dir
    # if not args.dp_out:
        # outputdir = f"{args.g_outdir}/DataProcessing"
        # assert os.path.exists(outputdir), \
                # f"Expected outputdir: {outputdir} does not exist"
    # else:
        # outputdir = args.dp_out
    # # -- Set default input dir
    # if not args.dp_in:
        # inputdir = f'{args.g_outdir}/NTuple'
        # assert os.path.exists(inputdir), \
                # f"Expected inputdir: {inputdir} does not exist"
    # else:
        # inputdir = args.dp_in

    # # -- Read sample file names
    # with open(args.samples_file,'r') as samples_file:
        # samples_name = samples_file.read().splitlines();

    # # -- As default create output dir
    # try:
        # os.makedirs(outputdir)
        # print(f'--> output dir {outputdir} created')
    # # -- if exist pass
    # except: pass
    # trees = ('Tree_Train','Tree_Val','Tree_Test')

    # # samples_file_path = samples_name[0]
    # for tree in trees:
        # print(f'--> Reading TTree: {tree}')
        # NTuple2Img = NTupleToImg(
                # # sample_name=samples_file_path,
                # stage='PREPROCESS',
                # # outdir=outputdir,
                # treename=tree,
                # );
        # NTuple2Img.loadTree_lazy(
                # # filepath=f'{NTuple2Img.InDir}/{samples_file_path}',
                # # filepath=f'{inputdir}/{samples_file_path}',
                # num_chunks=NTuple2Img.args.num_chunks,
                # )
        # # -- Free some memory, quite important step!
        # del NTuple2Img

