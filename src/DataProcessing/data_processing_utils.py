#!/usr/bin/env python3
#=============#
# Author: S. Noacco Rosende
# Contact: snoaccor@cern.ch
# GitLab:https://gitlab.cern.ch/snoaccor
# GitHub:https://github.com/santinoacco
#=============#
import numpy as np
import pandas as pd
import numba


@numba.njit
def opt_reshape(X,Eta,Phi):
    """
    Reshapes the array X to a new shape = (Phi,Eta,1)
    The reason to use this instead of a built-in function,
    is that we have greater control.

    # Args::
        X: (list/array)

        Eta:

        Phi:

    """
    mtrx = np.zeros(shape=(Phi,Eta))
    for phi in range(0,Phi):
        for eta in range(0,Eta):
            mtrx[phi][eta] = X[phi + Phi*eta]
    # -- up to here mtrx has a shape (Phi,Eta)
    #    however since we need (Phi,Eta,1) for the CNNs we might as well do it here.
    return mtrx.reshape(Phi,Eta,1)
    # return mtrx
def stand_alone_check_data_quality(data, outdir, do_studies="all"):
    pass

def remove_label_contamination(filepath,df):
    assert ('Sig' in filepath or 'Bkg' in filepath),\
        "Please make sure the path contain 'Sig' or 'Bkg' in their name to tell wich sample they hold"
    # -- label filter: only use Signal samples to get signal,
    #    same with Background
    if 'Sig' in filepath:
        # drop any non-signal samples
        filt = df["ph_isTruthMatch"] == False
    elif 'Bkg' in filepath:
        filt = df["ph_isTruthMatch"] == True
        # remove all rows where filt is True.
    df.drop(df.loc[filt].index,inplace=True)
    return df

def factors(n):    
    out = [(i, n//i) for i in range(1, int(n**0.5) + 1) if n % i == 0]
    return out

def get_best_raw_img_shape(cluster_size):
    """
    For a given Cluster size compute all its divisors,
    then remove the ones that are not useful to plot a matrix, for example (1,N) or in general a prime number.
    Finally choose the pairs whose elements distance is lowest,
    such that the matrix produced is as close to a square as possible.
    """
    perf_sqr = np.sqrt(cluster_size)
    if cluster_size % perf_sqr == 0:
        return [int(perf_sqr),int(perf_sqr)]
    
    else:
        all_factors = factors(cluster_size)
#         print(all_factors)
        # - remove 1 and cluster_size from list
        all_factors.remove((1,cluster_size))
#         print(all_factors)
        if len(all_factors)<1:
#             print('Size {0} is prime, skipping'.format(cluster_size))
            return np.nan
        # -- compute diference btw pair elem
        dif = [abs(p[0] - p[1]) for p in all_factors]
        # -- get index of minimum diference btwn factors
        min_idx = dif.index(min(dif))
        best_pair = all_factors[min_idx]
    
        # if the cluster_size is a prime nbr, this will return an empty list
        return best_pair

def do_reshape(clusters, mode=1, shape_for_TF=True, target_shape=None):
    """
    # Args
        - mode: (int) how to reshape the cluster.
            (0) "scrict": reshapes to a given target_shape.
            (1) "square": reshapes to a square shape
        - shape_for_TF: (bool) if True, change shape (A,B) -> (A,B,1).
    """
    valid_modes = {0:"strict_shape", 1:"closest_rect_to_sqr", 2:"square"}
    if not isinstance(clusters,pd.Series):
        clusters = pd.Series(clusters)
    assert mode in valid_modes.keys(), "Choose a mode from {0}".format(valid_modes.items())
    
    # -- get cluster sizes
    cluster_sizes = clusters.apply(len)
    # -- get all best posible target sizes
    target_shapes = cluster_sizes.apply(get_best_raw_img_shape)
    # -- remove nan values: these come from prime sizes
    clean_target_shapes = target_shapes.dropna()
    clean_clusters = clusters.where(target_shapes.notna()).dropna()    # note that .where replace false cases with np.nan
    
    if mode == 0:
        assert target_shape is not None, "This mode requires a specific target shape"
        assert isinstance(target_shape, tuple)
        if shape_for_TF:
            newshape = target_shape +(1,)
        else:
            newshape = target_shape
        try:
            reshaped_clusters = clusters.apply(np.reshape, args=((newshape,)))
        except err:
            raise err
    if mode == 1:
        aux_cont = []
        j=0
        for x, ts in zip(clean_clusters.values, clean_target_shapes.values):
            try:
                if shape_for_TF:
                    newshape = ts + (1,)
                else:
                    newshape = ts
                Xrshp = np.reshape(x, newshape)
            except TypeError:
                print("iter: ",j)
                print("arr\n",x)
                print("arr shape\n",x.shape)
                print()
                print(newshape)
                print()
                print("++++++++++++++++++++")
            aux_cont.append(Xrshp)
            j+=1
        reshaped_clusters = pd.Series(aux_cont)
    if mode == 2:
        raise NotImplementedError
    
    return reshaped_clusters

def get_filt_eq_targetsize(data, column, target_size):
    """
    Returns pd.Series of bools where is True if 
        size of data[column] == target_size
    """
    return data[column].apply(len) == target_size

def get_filt_btw_eta(data, eta_column='ph_Eta', abs_eta=(0,2.5)):
    """
    Returns pd.Series of bools where is True if 
        abs_eta[0] < data[eta_column].abs() <= abs_eta[1]
    """
    gr = data[eta_column].abs() > abs_eta[0]
    le = data[eta_column].abs() <= abs_eta[1]
    return gr & le

def NormalizeClusters(DF_Clusters, norm_type=1, use_min_offset=True):
    """
    # Args:
        
        - DF_Clusters: (PandasDataFrame)
        
        - norm_type: (int/list[floats])
            - 1: scale to cluster_E^{-1}. normalize by "area", sum of energy of all cells = "cluster energy".
                Divide by cluster energy
            - 2: scale to 1, divide by max cell energy.
            - 3: do norm_type_1 and then on top apply norm_type_2
            - 4: do norm_type_2 and then on top apply norm_type_1
            - arr(floats): 
                if given a float other than 1,2,3,4 it will use it as a custom cluster energy,
                and it will scale to that number.
        
        - use_min_offset: (bool)
    """
    if use_min_offset:
        # df of minimums from DF
        offset = DF_Clusters.apply(np.min)
        filt_neg_offsets = offset < 0
        DF_Clusters[filt_neg_offsets]+= offset[filt_neg_offsets].apply(np.abs)
        # now min should be >=0
        assert all(DF_Clusters.apply(np.min) >= 0), "Applying off-set to Clusters FAILED"
        
    if isinstance(norm_type,int):
        if norm_type == 1:
            sum_cells_E = DF_Clusters.apply(np.sum)
            Scaled_Cluster = DF_Clusters/sum_cells_E
        elif norm_type == 2:
            max_cell_E = DF_Clusters.apply(np.max)
            Scaled_Cluster = DF_Clusters/max_cell_E
        elif norm_type ==3:
            sum_cells_E = DF_Clusters.apply(np.sum)
            Prescaled_Cluster = DF_Clusters/sum_cells_E
            max_scaled_cell_E = Prescaled_Cluster.apply(np.max)
            Scaled_Cluster = Prescaled_Cluster/max_scaled_cell_E
        elif norm_type == 4:
            max_cell_E = DF_Clusters.apply(np.max)
            Prescaled_Cluster = DF_Clusters/max_cell_E
            sum_scaled_cell_E = Prescaled_Cluster.apply(np.sum)
            Scaled_Cluster = Prescaled_Cluster/sum_scaled_cell_E
    elif isinstance(norm_type,list) or isinstance(norm_type,np.ndarray()):
        norm_type = pd.Series(norm_type)
        # -- use norm_type value as scaling factor
        Scaled_Cluster = DF_Clusters/norm_type
    else:
        raise NotImplementedError
    
    return Scaled_Cluster


