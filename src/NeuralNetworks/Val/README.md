# Validation Overview

This script preforms the validation for a given model with a subsample of testing data.
The validation can be divided into two types: "*Inclusive*" and "*Binned*".
The last one means that all computations are carried out taking into account the (eta, pT) values of the photons,
while the first one does not.
So for each *type* we compute the ROC curves,
their AUC and the relevant WorkingPoints (WP).

For a given `<output>` the following directory tree will be created to store all results:

```
<output>
└── <usage>
    └── <strategy>
        └── <model_tag>
            ├── histograms
            └── roc_curves
                └── <sample_tag>
                    ├── plots
                    └── saved_data
```

## Run

-   `<config>`:: path to config file
-   `<output>`:: validation output dir
-   `<model_arch>`:: dir where the model is saved
-   `<model_wgts>`:: path to models weights (needed in case the full model wasn't saved.)

Lets consider different scenarios

### what model-saving-format is being used

Here we have to available possibilities:
1.  `.h5` format: which implies that there are two files,
    `<model_wgts>.h5` and `<model_arch>.json` 
2.  `.pb` format: also known as TensorFlow SavedModel format,
    which implies that there is one directory `<model_arch>` that keeps the necessary files.

To run in the 1. case:
* set `save_wgts_only = True` in the `<config>`, or pass it like an argument `--save_wgts_only True`.
* in the command line:
    > `$ ./val.py -c <config> -Parch <model_arch> -Pwgt <model_wgts>`

To run in the 2. case:
* set `save_wgts_only = False` in the `<config>`, or pass it like an argument `--save_wgts_only False`.
* in the command line:
    > `$ ./val.py -c <config> -Parch <model_arch>`

### how many files are being red [TOBE DEPRECATED]
If you have preformed a "chunk-trainning" then the full data to test is divided in a few files.
You can choose to read one or many, and to do so you only need to specify the `--num_files2read` argument (or set it in the `<config>`).
For now, if many files are red then for each file the ROC, AUC and WPs will be computed and then at the end they will be average,
as if they belonged to independent subsamples.

