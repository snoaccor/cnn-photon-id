#!/usr/bin/env python3
#=============#
# Authors: M. Belfkir, S. Noacco Rosende
# Contact: snoaccor@cern.ch
# GitLab:https://gitlab.cern.ch/snoaccor
# GitHub:https://github.com/santinoacco
#=============#
import numpy as np
import pandas as pd
import sys
import os
from datetime import date
import logging as log
sys.path.append('../../../')
# -- cnn-photon-id libraries
from src.common.script import Script
import src.NeuralNetworks.Models.models as M
import val_mod as Mod
from Plot import plot_mod as P
# -- Models
import tensorflow as tf
from tensorflow.keras import optimizers as opt
from tensorflow.keras import metrics
from tensorflow.keras.models import model_from_json
# -- Calls


#### NUMBA COMPILED ROUTINES ####
#################################

class Validation(Script):

    def __init__(self,stage='TEST',verbose=True):
        """
        The following implies the init of all the methods
        and properties set in Script.__init__()

        verbose: default is TRUE
            False: Silent
            True: Print basic info.
        """
        super().__init__(stage)
        self.verbose = verbose
        self._set_out_subdirs
        self._set_containers

    @property
    def _set_params(self):
        """ Configure Validation parameters """
        super()._set_params
        assert os.path.exists(self.args.path_2arch) and isinstance(self.args.path_2arch,str),\
                    "File doesn't exist or was defined incorrectly"
        if self.args.num_files2read is None:
            self.nf2r = self.args.num_chunks
        else:
            try:
                self.nf2r  = int(self.args.num_files2read)
            except ValueError:
                log.error("Number of files to read shall be type 'int'")
            assert self.nf2r <= self.args.num_chunks,\
                    f'Number of files to read shall be smaller or equal to \
                    {self.args.num_chunks}'
        self.step = str(self.args.val_step)
        self.path = str(self.args.path_2arch)
        self.path_to_model_wgts = str(self.args.path_2weights)
        self.load_full_model = not bool(self.args.save_wgts_only)
        if self.load_full_model==False:
            assert self.path_to_model_wgts is not None,\
                    "Expecting weights file path,\
                    set it in `config.ini` or pass it through '-Pwgt'."

        self.PT = [10,20,30,40,60,80,100,2000]
        if self.usage == 'OFFLINE':
            # NOTE: IF the following arr are gonna be unmutable
            #       then it would be better if they are sets or tuples.
            self.ETA = [0.,0.4,0.8,1.2,1.37,1.52,1.8,2.,2.2,2.5]
        elif self.usage == 'HLT':
            self.ETA = np.asarray([0., 0.6, 0.8, 1.15, 1.37, 1.52, 1.81, 2.01, 2.37, 2.47])   # <-- This is the REAL ARRAY WE SHOULD USE.

    @property
    def _set_containers(self):
        """
        WP_dict: (dict) it keeps the WorkingPoints data according to type(Binned/Inclusive) and usage

        ROC_dict: (dict) contains PandasDataFrames for the efficiecies to compute the ROCs

        # Args::None

        # Returns::None
        """
        # -- Set containers
        # ----- Inclusive in eta and pt containers
        self.WP_dict = {
                'Inclusive':{
                    'Offline_Tight':{
                    # 'Tight':{
                        'Sig_Eff':0.,
                        'Bkg_Rej':0.,
                        }
                    }
                }
        if self.usage == 'HLT':
            self.WP_dict['Inclusive'].update({
                    'HLT_FastReco_Tight':{
                    # 'HLT_FastReco':{
                        'Sig_Eff':0.,
                        'Bkg_Rej':0.,
                        }
                    })
        # ----- Binned in eta and pt containers
        for eta in self.ETA:
            for pt in self.PT:
                self.WP_dict[f'({eta};{pt})'] = {}
                for usage in self.WP_dict['Inclusive'].keys():
                    self.WP_dict[f'({eta};{pt})'][usage] = {
                            'Sig_Eff':0.,
                            'Bkg_Rej':0.,
                            }
        # ----- Binned in eta, Inclusive in pt; and
        #       Binned in pt, Inclusive in eta
        self.WP_SigEff_dict = {"Offline_Tight":{}}
        if self.usage == 'HLT':
            self.WP_SigEff_dict["HLT_FastReco_Tight"] = {}

        self.ROC_dict = {'Inclusive': pd.DataFrame()}
        for eta in self.ETA:
            for pt in self.PT:
                self.ROC_dict[f'({eta};{pt})'] = pd.DataFrame()

        self.AUC_dict = {'Inclusive':{}}
        for eta in self.ETA:
            for pt in self.PT:
                self.AUC_dict[f'({eta};{pt})'] = {}

        self.SigEff_at_BkgRej_dict = {"HLT_FastReco_Tight":{}}

        # -- Init matrices
        mat_dim = (len(self.PT)-1,len(self.ETA)-1,3)
        self.Auc_Inc = np.zeros(mat_dim) # the zeros let you sum lists

        # TODO: implement these ones! to do so 'maxEff' and 'scanEff' should work.
        # Bkg_Inc = np.zeros(mat_dim)
        # Sig_Inc = np.zeros(mat_dim)
        # CNN_Bkg_Inc = np.zeros(mat_dim)
        # CNN_Sig_Inc = np.zeros(mat_dim)
        # Bkg_Imp_Inc = np.zeros(mat_dim)
        # Sig_Imp_Inc = np.zeros(mat_dim)


    @property
    def _set_tag(self):
        """Uses Trained Tag which should be passed by the args.path_2arch arg"""
        full_path_arr = self.path.split('/')
        try:
            for p in full_path_arr:
                if 'CL' in p:
                # -- NOTE: I am using a pair of letters that should be in the train tag.
                    self.tag = p
            if self.verbose: print('CHECKING TAG',self.tag)
            if self.tag is None:
                raise FileNotFoundError
        except FileNotFoundError:
            print("Can't find model")

    def _crack_cond(self, eta):
        return 1.37 <= eta < 1.52

    def _not_enough_phs(
            self, y_true_masked,
            eta=None, eta_next=None,
            pt=None, pt_next=None):
        if y_true_masked.shape[0] == 0:
            log.warning(f'**> No photons in range ({eta},{eta_next})x({pt},{pt_next})... skipping')
            return True
        else:
            return False

    def _not_enough_representation(self,y_true_masked):
        unique_categories =  np.unique(y_true_masked)
        if unique_categories.shape[0] < 2:
            log.warning('**> No samples of both categories, can not compute ROC... skipping')
            return True
        else:
            return False

    def load_model_predictions(self,path=None):
        pred_path = '/'.join([self.args.g_outdir,"Predict",self.usage,f"stg_{self.stg}",self.tag])
        if path is None:
            # path =  '{0}/Y_pred.npy'.format(self.InDir)
            path =  '{0}/Y_pred.npy'.format(pred_path)
        return np.load(file=path,allow_pickle=True)

    def load_wps_predictions(self,path=None):
        pred_path = '/'.join([self.args.g_outdir,"Predict",self.usage,f"stg_{self.stg}",self.tag])
        if path is None:
            # path =  '{0}/WP_pred.h5'.format(self.InDir)
            path =  '{0}/WP_pred.h5'.format(pred_path)
        return pd.read_hdf(path,"WP_pred")

    def load_input(self,skip_X=False):
        for chk in range(0, self.nf2r):
            log.info(f'--> loading file {chk + 1}/{self.nf2r}')
            if self.verbose: print(f'--> loading file {chk + 1}/{self.nf2r}')
            # --------------- #
            self.load_data(chunk=chk,skip_X=skip_X)
        Y_true = self.data.Y_Test
        Y_pred = self.load_model_predictions()
        # WP_pred = self._get_predicted_WPs()
        WP_pred = self.load_wps_predictions()
        # log.debug(f'Sizes YT: {Y_true.shape[0]} YP: {Y_pred.shape[0]} WP: {WP_pred.shape[0]}')
        return Y_true, Y_pred, WP_pred

    def compute_SigBkg_Eff_MCdata(self, y_true, z_true):
        """
        y_ture: (NumpyArray), labels true value
        z_true: (NumpyArray), WorkingPoints true values.

        ## Returns:

            Sig_Eff: of the WPs

            Bkg_Rej: of the WPs
        """
        if not isinstance(y_true,np.ndarray):
            y_true = np.asarray(y_true)
        if not isinstance(z_true,np.ndarray):
            z_true = np.asarray(z_true)
        TP,TN,FP,FN = Mod._optimized_masking_confusion_mat(y_true,z_true)
        Sig, Bkg = Mod._fix_ZeroDiv(TP,TN,FP,FN)
        return Sig, Bkg


    # -------------------- #
    def compute_step(self):
        pass

    def _compute_ALL(self, y_true, y_pred, wp_pred, wp_dict, roc_df, auc_dict, bins=None):
        """
        computes the ROC curve and its AUC,
        it appends the respective WPs values

        # Args::
            wp_pred: (dict), contains the data we need to compute the WPs.
            wp_dict: (dict), assuming it has this shape: {<usage>:{'Sig_Eff':[] or float?}},
                it's the container where the binned WP result will be saved.

        # Returns::
            roc_df: (PandasDataFrames)
            auc_dict: (dict)
        """
        for usage in wp_dict.keys():
            wp_sigeff, wp_bkgrej = self.compute_SigBkg_Eff_MCdata(
                    y_true=y_true,
                    z_true=wp_pred[usage]
                    )
            wp_dict[usage]['Sig_Eff'] += wp_sigeff
            wp_dict[usage]['Bkg_Rej'] += wp_bkgrej

        # -- Compute the ROC data
        roc_df, auc_dict = Mod.compute_ROC(
                y_true      = y_true,
                y_pred      = y_pred,
                roc_df      = roc_df,
                auc_dict    = auc_dict
                )

        return roc_df, auc_dict

    # TODO: profile and check if numba can improve this method.
    def _mat_additem(self, matrix, auc_data, row, col):
        elem = [
                auc_data['ROC_AUC_ErrL'],
                auc_data['ROC_AUC'],
                auc_data['ROC_AUC_ErrH']
                ]
        # NOTE
        # we use a += because in the case the matrix is already filled we don't want to
        # delete its content, we will later take the average
        matrix[row,col] += elem

    def _get_masked_arr(self,y_true,y_pred,wp_pred,etabin=None,ptbin=None):
        """
        Applies a cut in eta and pT to mask the containers so that we only use the data that belongs to (etabin,ptbin).

        # Args::
            y_true:
            y_pred:
            wp_pred:
            etabin:
            ptbin:

        # Returns::
            Y_true_masked:
            Y_pred_masked:
            WP_pred_masked: (dict), for each usage it holds the WorkingPoints values according
            to the (etabin,ptbin) being consider.
        """
        WP_pred_masked={}
        mask = Mod._pT_Eta(
                PT=self.PT, ETA=self.ETA,
                data=self.data,
                ptbin=ptbin, etabin=etabin,
                )

        # -- Set masked variables
        Y_true_masked= y_true[mask];
        Y_pred_masked= y_pred[mask];
        WP_pred_masked['Offline_Tight'] = wp_pred['Offline_Tight'][mask];
        if self.usage == 'HLT':
            WP_pred_masked['HLT_FastReco_Tight'] = wp_pred['HLT_FastReco_Tight'][mask];

        return Y_true_masked, Y_pred_masked, WP_pred_masked

    def execute(self, Y_true, Y_pred, WP_pred,step='sigeff'):
        """
        Executes all computation for each file (subsample) of data.
        It first computes in "Inclusive" mode and then in "Binned".

        # Args::
            Y_true:
            Y_pred:
            WP_pred:
            verbose:

        # Returns:: None
        """
        if step == "rocauc":
            # -- Inclusive
            log.info(" --> Executing Inclusive ")
            self.ROC_dict['Inclusive'], self.AUC_dict['Inclusive'] = self._compute_ALL(
                    Y_true, Y_pred, WP_pred,
                    self.WP_dict['Inclusive'],
                    self.ROC_dict['Inclusive'],
                    self.AUC_dict['Inclusive']
                    )
            # # FIXME
            # print(" *** Testing scanEff ***")
            # # print(Y_true.shape[0],type(Y_true))
            # CNN_SigEff_Tight, CNN_BkgRej_Tight, CNN_cut = Mod.scanEff(
                    # y_true=Y_true,
                    # y_pred=Y_pred,
                    # # y_true=np.asarray(Y_true),
                    # # y_pred=np.asarray(Y_pred),
                    # true_Sig_Eff=WP_pred['Tight'],
                    # )

            # -- Binned ROC & AUC
            WP_pred_masked={}
            for (etabin, eta) in enumerate(self.ETA):
                if self.verbose: print(' --> reading eta bin',etabin)
                if self._crack_cond(eta):
                    log.warning(f'** Crack Region -> skipping bin {etabin} **')
                    continue
                for (ptbin, pt)  in enumerate(self.PT):
                    if etabin+1 >= len(self.ETA) or ptbin+1 >= len(self.PT): continue
                    if self.verbose: print(f' --> Masking ({etabin};{ptbin})')
                    Y_true_masked, Y_pred_masked, WP_pred_masked = self._get_masked_arr(
                            Y_true, Y_pred, WP_pred,
                            etabin, ptbin
                            )

                    # -- Adress some Issues:
                    if self._not_enough_phs(Y_true_masked,eta, self.ETA[etabin+1],pt, self.PT[ptbin+1]): continue
                    if self._not_enough_representation(Y_true_masked): continue

                    log.info(f' --> Filling AUC matrix for ptbin={ptbin}, etabin={etabin}')
                    if self.verbose:
                        print(f' --> Filling AUC matrix for ptbin={ptbin}, etabin={etabin}')
                    # -- Compute WPs efficiency for each (pT,eta) bin
                    self.ROC_dict[f'({eta};{pt})'], self.AUC_dict[f'({eta};{pt})'] = self._compute_ALL(
                            Y_true_masked, Y_pred_masked, WP_pred_masked,
                            self.WP_dict[f'({eta};{pt})'],
                            self.ROC_dict[f'({eta};{pt})'],
                            self.AUC_dict[f'({eta};{pt})']
                            )

                    self._mat_additem(
                            matrix=self.Auc_Inc,
                            auc_data=self.AUC_dict[f'({eta};{pt})'],
                            row=ptbin, col=etabin
                            )


                    # print(f"Sig Eff : {CNN_Sig_Eff_Tight}, Bkg Rej : {CNN_Bkg_Eff_Tight}, Cut : {CNN_cut}, ATLAS Eff : {Sig_Eff_Tight}, ATLAS Rej : {Bkg_Eff_Tight}")
                    # FIXME
                    # Z, X, S, B = Mod.maxEff(y_true=Y_true, y_pred=Y_pred)
                    # P.plot_scan(Z, X, S, B, 'Inclusive',self.model_dir)

        elif step == "sigeff":
            # -- Sig Eff at Bkg Rej of WP
            self.compute_SigEff_at_WPsBkgRej(Y_true, Y_pred, WP_pred)

    
    # TODO: WIP
    def compute_SigEff_at_WPsBkgRej(self, Y_true, Y_pred, WP_pred):
        # -- compute for eta
        WP_pred_masked={}
        # self.SigEff_at_BkgRej_dict["eta"] = {}
        eta_lst = []
        wp_sigeff_lst = []
        sig_eff_at_WPbkgrej_lst = []
        for (etabin, eta) in enumerate(self.ETA):
            if self.verbose: print(' --> reading eta bin',etabin)
            if self._crack_cond(eta):
                log.warning(f'** Crack Region -> skipping bin {etabin} **')
                continue
            if etabin+1 >= len(self.ETA): continue
            # if self.verbose: print(f' --> Masking bins ({etabin};{ptbin})')
            # -- mask values for eta
            Y_true_masked, Y_pred_masked, WP_pred_masked = self._get_masked_arr(
                    Y_true, Y_pred, WP_pred, etabin
                    )
            # -- Adress some Issues:
            if self._not_enough_phs(Y_true_masked, eta, self.ETA[etabin+1]): continue
            if self._not_enough_representation(Y_true_masked): continue

            # -- Append eta
            eta_lst.append(eta)
            # print(f"Pred WP eta mask type: {type(WP_pred_masked)}")
            # print(f"Pred WP eta mask: {WP_pred_masked}")
            # -- Get WP bkg rej lvl for eta.
            wp_sigeff, _wp_bkgrej = self.compute_SigBkg_Eff_MCdata(
                    y_true=Y_true_masked,
                    z_true=WP_pred_masked["HLT_FastReco_Tight"]
                    # y_true=Y_true_masked,
                    # z_true=WP_pred_masked 
                    )
            # -- save WP Signal eff for plotting purposes.
            # NOTE: for now only vs HLT_FASTRECO

            wp_sigeff_lst.append(wp_sigeff)
            # self.WP_SigEff_dict["HLT_FastReco_Tight"][eta] += wp_sigeff
            # self.WP_SigEff_dict["HLT_FastReco_Tight"][eta].update({eta:wp_sigeff}) 

            # -- Compute Signal Efficiency at WP Background Rejection
            sig_eff_at_WPbkgrej = Mod.get_SigEff_at_BkgRej(
                    y_true = Y_true_masked,
                    y_pred = Y_pred_masked,
                    bkg_rej_lvl = _wp_bkgrej
                    )
            sig_eff_at_WPbkgrej_lst.append(sig_eff_at_WPbkgrej) 

        self.SigEff_at_BkgRej_dict["HLT_FastReco_Tight"]['eta']=(eta_lst,sig_eff_at_WPbkgrej_lst)
        self.WP_SigEff_dict["HLT_FastReco_Tight"]['eta']=(eta_lst,wp_sigeff_lst)

        # # -- compute for pt
        # for (ptbin, pt)  in enumerate(self.getattr(var.upper():
        # for (ptbin, pt)  in enumerate(self.PT):
            # if self.verbose: print(' --> reading pt bin',ptbin)
            # if ptbin+1 >= len(self.PT): continue

        # pass


    # -- SAVING METHODS -- #
    def _save_SigBkg_Eff_MCdata(self, Sig, Bkg, WP_label, sample_tag, bins=None):
        """"""
        outdir = f'{self.roc_dir}/{sample_tag}/saved_data'
        try:
            os.makedirs(outdir)
        except FileExistsError:
            pass

        if bins is None:
            fname = os.path.join(outdir,f'full_{WP_label}')
        else:
            fname = os.path.join(
                    outdir,
                    'eta{eta:1.2f}xpT{pt:1.1f}_{label}'.format(
                        eta=bins[0],
                        pt=bins[1],
                        label=WP_label
                        )
                    )

        np.save(arr=Sig,file=f'{fname}_SigEff',allow_pickle=True)
        np.save(arr=Bkg,file=f'{fname}_BkgRej',allow_pickle=True)

    def _save_ROC(self, save_DF, sample_tag, bins=None):
        outdir = f'{self.roc_dir}/{sample_tag}/saved_data'
        if bins is None:
            fname = os.path.join(outdir,'full_outROC')
        else:
            fname = os.path.join(
                    outdir,
                    'eta{eta:1.2f}xpT{pt:1.1f}_outROC'.format(
                        eta=bins[0],
                        pt=bins[1],)
                    )
        Mod.save_data(
                data=save_DF,
                out_file_path=fname,
                fmt='NPY'
                )

    def save_AUC_mat(self, mat, sample_tag):
        outdir = f'{self.roc_dir}/{sample_tag}/saved_data'
        fname = os.path.join(outdir,f'BinnedAUC_Matrix.npy')
        np.save(arr=mat,
                file=fname,
                allow_pickle=True)

    def save_results(self, wp_dict, roc_df, sample_tag, bins):
        for usage in wp_dict.keys():
            self._save_SigBkg_Eff_MCdata(
                    Sig = wp_dict[usage]['Sig_Eff'],
                    Bkg = wp_dict[usage]['Bkg_Rej'],
                    WP_label=usage,
                    sample_tag=sample_tag,
                    # OutDir=self.roc_dir,
                    bins=bins
                    )
        # -- Save ROC data to binaries
        self._save_ROC(
                save_DF = roc_df,
                sample_tag = sample_tag,
                bins = bins,
                )

    def save_ALL(self, sample_tag):
        self.save_results(
                wp_dict=self.WP_dict['Inclusive'],
                roc_df=self.ROC_dict['Inclusive'],
                sample_tag= sample_tag,
                bins=None
                )
        for (etabin, eta) in enumerate(self.ETA):
            for (ptbin, pt)  in enumerate(self.PT):
                if self.nf2r > 1:
                    self.average_ROC_results(
                            roc_df = self.ROC_dict[f'({eta};{pt})'],
                            num_subsamples = num_subsamples
                            )
                if self.ROC_dict[f'({eta};{pt})'].shape[0] == 0:
                    continue

                log.info(f" --> Saving ({eta};{pt}) data ")
                if self.verbose:
                    print(f" --> Saving ({eta};{pt}) data ")
                self.save_results(
                        wp_dict=self.WP_dict[f'({eta};{pt})'],
                        roc_df=self.ROC_dict[f'({eta};{pt})'],
                        sample_tag=sample_tag,
                        bins=(eta,pt)
                        )
        # -- Save the AUC_Matrix
        log.info(f" --> Saving AUC_Matrix ")
        if self.verbose:
            print(f" --> Saving AUC_Matrix ")
        self.save_AUC_mat(
                mat=self.Auc_Inc,
                sample_tag=sample_tag
                )

    def plot_ALL(self, Y_true, Y_pred, WP_pred, sample_tag, AUC_plotsty='heatmap',step='sigeff'):
        if step=="rocauc":
            # -- ROC curves
            log.info(" --> Ploting Inclusive ROC curve")
            if self.verbose: print(" --> Ploting Inclusive ROC curve")
            P.plot_roc_curve(
                    roc_data=self.ROC_dict['Inclusive'],
                    auc_dict=self.AUC_dict['Inclusive'],
                    wp_dict=self.WP_dict['Inclusive'],
                    sample_tag=sample_tag,
                    OutDir=self.roc_dir,
                    )
            for (etabin, eta) in enumerate(self.ETA):
                for (ptbin, pt)  in enumerate(self.PT):
                    if self.ROC_dict[f'({eta};{pt})'].shape[0] == 0:
                        continue

                    P.plot_roc_curve(
                            roc_data=self.ROC_dict[f'({eta};{pt})'],
                            auc_dict=self.AUC_dict[f'({eta};{pt})'],
                            wp_dict=self.WP_dict[f'({eta};{pt})'],
                            sample_tag=sample_tag,
                            OutDir=self.roc_dir,
                            bins=(eta,pt)
                            )

            # -- Sig&Bkg Histograms for CNN
            P.plot_CNN_output(
                    y_true = Y_true,
                    y_pred = Y_pred,
                    sample_tag = 'MC_data',
                    OutDir = f'{self.histo_dir}',
                    )

            # -- Set plot Labels for AUC matrix
            EtaLabels = ['[{0:.2f},{1:.2f})'.format(self.ETA[i],self.ETA[i+1])\
                    for i in range(len(self.ETA)-1)]
            PTLabels = ['[{0:.0f},{1:.0f})'.format(self.PT[i],self.PT[i+1])\
                    for i in range(len(self.PT)-1)]

            # -- AUC Matrix
            P.plot_AUC_matrix(
                    data=self.Auc_Inc,
                    row_labels=PTLabels,
                    col_labels=EtaLabels,
                    title='AUC_Mode2',
                    plotstyle=AUC_plotsty,
                    cbarlabel='norm mean AUC',
                    OutDir=f'{self.roc_dir}/Inclusive/plots',
                    )

        elif step=="sigeff":
            # -- Signal Efficiency at WP Background Rejection Level
            P.plot_signal_eff(var='eta',
                    model_dict=self.SigEff_at_BkgRej_dict["HLT_FastReco_Tight"],
                    wp_dict=self.WP_SigEff_dict['HLT_FastReco_Tight'],
                    sample_tag="Inclusive",
                    OutDir=self.roc_dir,
                    )

        # TODO: implement
        # P.plot_matrix(
                # matrix=Sig_Inc,
                # title='Sig_Eff_Inclusive',
                # OutDir=f'{self.roc_dir}/Inclusive/plots')
        # plot_matrix(CNN_Bkg_Inc, 'CNN_Bkg_Rej_Inclusive')
        # plot_matrix(CNN_Sig_Inc, 'CNN_Sig_Eff_Inclusive')
        # plot_matrix(Bkg_Imp_Inc, 'Bkg_Imp_Inclusive')
        # plot_matrix(Sig_Imp_Inc, 'Sig_Imp_Inclusive')


def main():
    # -- Config logging
    log.basicConfig(
            filename='validation_run.log',
            filemode='w',
            level=log.INFO
            )
    # logger = log.getLogger(__name__)
    log.info(" --> Starting Validation")
    val = Validation()

    Y_true, Y_pred, WP_pred = val.load_input(skip_X=True)
    val.execute(Y_true, Y_pred, WP_pred)

    log.info(" --> Saving all data ")
    val.save_ALL(sample_tag='Inclusive')
    log.info(" --> Plotting all data ")
    val.plot_ALL(Y_true, Y_pred, WP_pred, sample_tag='Inclusive')
    log.info(" --> ** Successfull Validation ended **")
    if val.verbose:
        print(" --> ** Successfull Validation ended **")

if __name__ == '__main__':
    main()


    # log.info(" --> averaging results over chunks")
    # if val.nf2r > 1:
        # val.average_ROC_results(
                # roc_df=val.ROC_dict['Inclusive'],
                # num_subsamples=num_subsamples
                # )
        # val.normalice_AUC_results(
                # AUC_Matrix=val.Auc_Inc,
                # num_subsamples=num_subsamples
                # )

    # def average_ROC_results(self, roc_df, num_subsamples):
        # # -- Compute the mean between values with same index
        # roc_df = roc_df.groupby(
                # by=roc_df.index,
                # level=0
                # ).mean()
        # # -- std should be mean(std)/sqrt(num_chunks)
        # roc_df.loc[
                # ['ROC_fprErrL','ROC_fprErrH','ROC_tprErrL','ROC_tprErrH']
                # ]/=np.sqrt(num_subsamples)

    # def normalice_AUC_results(self, AUC_matrix, num_subsamples):
        # """
        # Assuming AUC_matrix is filled with AUC data from 'num_subsamples' different files.
        # This method normilices the values to be interpreted as:
        # (meanAUC, lowerStdAUC, higerStdAUC)
        # """
        # # first element shuld be the mean, at this stage we are missing the normalization so.
        # AUC_matrix[:,:,0] /= num_subsamples
        # # second and theird shuold be the standar deviation lower and upper limits
        # AUC_matrix[:,:,1:] /= np.sqrt(num_subsamples)

        # return AUC_matrix

