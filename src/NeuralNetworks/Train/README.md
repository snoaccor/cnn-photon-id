# Documentation on Train/

## train.py

Training on the model is done here.
The general configuration should be set in the [`<config_file>`](../../../run/config.ini),
although it could all be implemented through command line argument parsing;
this option tends to be less tidy.
Some capabilities are restricted to the _TensorFlow_ version being used.

The models are defined [here](../Models/models.py).

To add a _new_ metric: 
* add the metric to `Train.available_metrics`
* add an entry to the `metrics` dictionary at the config file,
  using the metrics name as the key.

To add a _new_ callback:
* add the metric to `Train.set_available_callbacks`
* add an entry to the `callbacks` dictionary at the config file,
  using the callback name as the key.

-   TODO: define a function to set an modify the hyperparameters space from the config

### run

_**The overall command is**_:

```shell
./train.py -c <config> --args
```

For `HLT` there are 3 different types of runs one can preform.
-   TODO: extend to `OFFLINE`
-   TODO: replace Generator for [`tf.TFRecord`](https://www.tensorflow.org/tutorials/load_data/tfrecord),
    because it allows for multithreading.

- a.  #### Train a costume model

    Use the `arch` dictionary in the config file along with the `models.py` file,
    to uniquely specify the model to train.

- b.  #### Search models using KerasTuner

    -   Set `use_keras_tuner = True`
    -   Use the `arch` dictionary in the config file to define the search space of hyperparameters.
    The values of the dictionary set the maximum value of the hyperparameter search.
    For now the `RandomSearch` is the only tuner implemented,
    however it is easy to use another one by modifying `Train.set_tuner`.

- c.  #### (Re)Train a model found using KerasTuner
    Once you have trained using KerasTuner,
    the best models' hyperparameters will be saved in a `best_hp` file,
    you can retrain the model using those hyperparameters on a larger sample,
    for instance.

    ```shell
    ./train.py -c <config> --load_best_hp <path_to_best_hp.pkl>
    ```

### Generator training
%%In addition to the different type of runs,
%%one could preform the training in "chunks",
%%meaning at chunks of files per training loop.
%%This terns out to be a usefull option when your sample is to big to be loaded in memory all at once.
%%If you are doing a KerasTuner search,
%%we recommend to use a smaller (yet significant) subsample to tune untill you get your top models.
%%You can then retrain with the chosen models over the whole sample implementing the chunk-training.

### Saving

As a default and for a given Global-Output-Dir (`<g_outdir>`) this package will create the following dirs:
> g_outdir/usage/strategy/model_tag/
>>  history/ \
>>  saved_models/

In the "saved_models" folder you will find the checkpoints, the models_architecture and the best hyperparameters (if required).

### Plotting

to plot all the training-history saved at the directory mention above,
you can run [`plot_history`](../../Plot/plot_history.py).



## toONNX.py

Deploys your model `.h5` to `.onnx` so it can be use on an ATHENA framework.

-   ### TODO-List
    -   [] implement package Script structure.


##  reweighting.py

-   ### TODO-List
    -   [] implement package Script structure.
    -   [] Review it.
