#!/usr/bin/env python3
#=============#
# Authors: M. Belfkir, S. Noacco Rosende
# Contact: snoaccor@cern.ch
# GitLab:https://gitlab.cern.ch/snoaccor
# GitHub:https://github.com/santinoacco
#=============#

import numpy as np
import pandas as pd
import tensorflow as tf
import predict_utils
import os
import sys
import logging as log
sys.path.append('../../../')
from src.common.script import Script


class Predict(Script):

    def __init__(self,stage='PREDICT',verbose=True):
        """
        The following implies the init of all the methods
        and properties set in Script.__init__()

        verbose: default is TRUE
            False: Silent
            True: Print basic info.
        """
        super().__init__(stage)
        self.verbose = verbose
        self._set_out_subdirs

    @property
    def _set_params(self):
        """ Configure Predict parameters """
        super()._set_params
        assert os.path.exists(self.args.path_2arch) and isinstance(self.args.path_2arch,str),\
                    "File doesn't exist or was defined incorrectly"
        if self.args.num_files2read is None:
            self.nf2r = self.args.num_chunks
        else:
            try:
                self.nf2r  = int(self.args.num_files2read)
            except ValueError:
                log.error("Number of files to read shall be type 'int'")
            assert self.nf2r <= self.args.num_chunks,\
                    f'Number of files to read shall be smaller or equal to \
                    {self.args.num_chunks}'
        self.step = str(self.args.val_step)
        self.path = str(self.args.path_2arch)
        self.path_to_model_wgts = str(self.args.path_2weights)
        self.load_full_model = not bool(self.args.save_wgts_only)
        if self.load_full_model==False:
            assert self.path_to_model_wgts is not None,\
                    "Expecting weights file path,\
                    set it in `config.ini` or pass it through '-Pwgt'."

        self.PT = [10,20,30,40,60,80,100,2000]
        if self.usage == 'OFFLINE':
            # NOTE: IF the following arr are gonna be unmutable
            #       then it would be better if they are sets or tuples.
            self.ETA = [0.,0.4,0.8,1.2,1.37,1.52,1.8,2.,2.2,2.5]
        elif self.usage == 'HLT':
            self.ETA = np.asarray([0., 0.6, 0.8, 1.15, 1.37, 1.52, 1.81, 2.01, 2.37, 2.47])   # <-- This is the REAL ARRAY WE SHOULD USE.
    
    @property
    def _set_tag(self):
        """Uses Trained Tag which should be passed by the args.path_2arch arg"""
        full_path_arr = self.path.split('/')
        try:
            for p in full_path_arr:
                if 'CL' in p:
                # -- NOTE: I am using a pair of letters that should be in the train tag.
                    self.tag = p
            if self.verbose: print('CHECKING TAG',self.tag)
            if self.tag is None:
                raise FileNotFoundError
        except FileNotFoundError:
            print("Can't find model")

    def _crack_cond(self, eta):
        return 1.37 <= eta < 1.52

    def get_Lr2HLT_passcut(self, ph_eta, ph_pT, ph_Reta, ph_Rhad, ph_Eratio):
        """ wrapper to call the optimized `get_Lr2HLT_passcut_fast` method """
        out = predict_utils.get_Lr2HLT_passcut_fast(
                self.ETA, ph_eta, ph_pT,
                ph_Reta, ph_Rhad, ph_Eratio)
        return out

    def load_model(self):
        """
        Loads the model from the saved files,
        two options available:
        >   Load Full Model: if the model was saved under the TF_SavedModel format.
        >   Load Weights: if the model's architecture as saved as a .json file and the model's weights as a .h5 file.
        """
        if self.load_full_model:
            clf = tf.keras.models.load_model(filepath=self.path)
        else:
            with open(self.path,'r') as json_file:
                loaded_from_json = json_file.read()
                clf = tf.keras.models.model_from_json(loaded_from_json)
                clf.load_weights(self.path_to_model_wgts)

        log.info(' --> Model Reloded ')
        if self.verbose: print(' --> Model Reloded ')
        return clf
    
    def _get_Xtest(self):
        """ Get Test Data """
        X2_test = self.data.X2_Test
        # -- Check shapes are fine
        X2_test_shape = (X2_test.shape[0],) + self.CaloLr2Shape + (1,)
        X2_test = self.assert_reshape(X2_test,X2_test_shape)
        if self.usage == 'OFFLINE':
            # --Get Test Data
            X1_test = self.data.X1_Test
            X3_test = self.data.X3_Test
            # -- Check shapes are fine
            X1_test_shape = (X1_test.shape[0],) + self.CaloLr1Shape + (1,)
            X3_test_shape = (X3_test.shape[0],) + self.CaloLr3Shape + (1,)
            X1_test = self.assert_reshape(X1_test,X1_test_shape)
            X3_test = self.assert_reshape(X3_test,X3_test_shape)

            X_test = {'Input1': X1_test, 'Input2': X2_test, 'Input3':X3_test}
        elif self.usage == 'HLT':
            X_test = X2_test
        else:
            raise RuntimeError("No testing data available")
        return X_test
    
    def _get_predicted_WPs(self):
        """
        Gets the WorkingPoints values for the dataset

        # Args::None

        # Returns::
            WP_pred: (dict) keys are `usage`, it stores the WorkingPoints data as NumpyArrays.
        """
        WP_pred = {}
        WP_pred['Offline_Tight'] = self.data.Z_Test['isEMTightIncOff'].to_numpy();
        if self.usage == 'HLT':
            # NOTE: If usage is HLT then for now you need the following variables to get the WP
            ph_pT  = self.data.Z_Test['pt'].to_numpy()
            ph_eta = self.data.Z_Test['eta'].to_numpy()
            Rhad   = self.data.Z_Test['Rhad'].to_numpy()
            Reta   = self.data.Z_Test['Reta'].to_numpy()
            f1     = self.data.Z_Test['f1'].to_numpy()
            Eratio = self.data.Z_Test['Eratio'].to_numpy()

            # -- Get array of WP cut results for data.
            HLT_pred = self.get_Lr2HLT_passcut(
                    ph_eta=ph_eta,
                    ph_pT=ph_pT,
                    ph_Reta=Reta,
                    ph_Rhad=Rhad,
                    ph_Eratio=Eratio
                    )
            WP_pred['HLT_FastReco_Tight'] = HLT_pred;
            # -- I'm not sure if this is deletion is necessary.
            del ph_pT, ph_eta, Rhad, Reta, f1, Eratio
        return WP_pred
    
    def get_predictions(self, model):
        # -- Get predictions from model
        X_Test = self._get_Xtest()
        Y_pred = model.predict(X_Test)
        Y_pred = Y_pred.reshape(Y_pred.shape[0],1)
        return Y_pred

    def save_model_predictions(self,y_pred,outpath=None):
        if outpath is None:
            outpath = '{0}/Y_pred'.format(self.model_dir)
        np.save(file=outpath,arr=y_pred)

    def save_WPs_predictions(self,wp_pred,outpath=None):
        if outpath is None:
            outpath = "{0}/WP_pred.h5".format(self.model_dir)
        wp_pred.to_hdf(outpath, "WP_pred")

    def execute(self):
        """
        Generates the predictions for a given model and set of X Test data.
        """

        # -- Load model
        model = self.load_model()

        # -- Gen predictions from test data.
        Y_pred=np.empty(0)
        for chk in range(0, self.nf2r):
            log.info(f'--> loading file {chk + 1}/{self.nf2r}')
            if self.verbose: print(f'--> loading file {chk + 1}/{self.nf2r}')
            # --------------- #
            self.load_data(chunk=chk)
            log.info("--> predicting")
            Y_pred = np.append(arr=Y_pred, values=self.get_predictions(model))
        WP_pred_df = pd.DataFrame(self._get_predicted_WPs())

        log.info("Saving predictions")    
        self.save_model_predictions(y_pred=Y_pred)
        self.save_WPs_predictions(WP_pred_df)


def main():
    # -- Config logging
    log.basicConfig(
            filename='predictions_run.log',
            filemode='w',
            level=log.INFO
            )
    pred = Predict()
    pred.execute()



if __name__ == '__main__':
    main()


