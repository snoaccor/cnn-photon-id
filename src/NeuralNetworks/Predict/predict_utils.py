#!/usr/bin/env python3

import numba
import numpy as np

@numba.njit
def get_Lr2HLT_passcut_fast(ETA, ph_eta, ph_pT, ph_Reta, ph_Rhad, ph_Eratio, threshold=25, ph_F1=None):
    """
     Set trigger cuts for eta bins according to a given threshold.
     For now it's only Medium Cuts

    # Args::
        ETA:

        ph_eta:

        ph_pT:

        ph_Rhad:

        ph_Reta:

        ph_Eratio:

        ph_F1:

        threshold:

    # Returns::
    """
    abs_ph_eta = np.abs(ph_eta)

    trigger_F1Cut = [0.005]
    if (20. <= threshold < 30):
        trigger_Rhad_trsd   = np.asarray([0.071, 0.062, 0.075, 0.060, 0.051, 0.057, 0.075, 0.072, 0.051])
        trigger_Reta_trsd   = np.asarray([0.819375, 0.819375, 0.800375, 0.828875, 0.7125, 0.805125, 0.843125, 0.824125, 0.700625])
        trigger_Eratio_trsd = -999*np.ones(9)

    if (30. <= threshold < 40):
        trigger_Rhad_trsd   = np.asarray([0.071, 0.062, 0.075, 0.060, 0.051, 0.057, 0.075, 0.072, 0.051])
        trigger_Reta_trsd   = np.asarray([0.819375, 0.819375, 0.800375, 0.828875, 0.7125, 0.805125, 0.843125, 0.824125, 0.700625])
        # trigger_Eratio_trsd = [-999., -999., -999., -999., -999., -999., -999., -999., -999.]
        trigger_Eratio_trsd = -999*np.ones(9)

    IBIN = []
    # -- Get index to check cut given by the ph_eta value.
    # NOTE: THERE MUST BE A WAY TO MAKE THIS MORE EFFICIENTLY
    for i in range(ETA.shape[0]-1):
        for j in range(ph_eta.shape[0]):
            cond = (ETA[i] <= abs_ph_eta[j]) and (abs_ph_eta[j] < ETA[i+1])
            if cond:
                # -- Get etabin given ph_eta
                IBIN.append(i)

    # log.debug(f'IBIN size = {len(IBIN)}', f'ex IBIN: {IBIN[:30]}')

    # -- Map cuts to ph
    trig_Reta_Cut = np.asarray([trigger_Reta_trsd[i] for i in IBIN])
    trig_Rhad_Cut = np.asarray([trigger_Rhad_trsd[i] for i in IBIN])
    trig_Eratio_Cut = np.asarray([trigger_Eratio_trsd[i] for i in IBIN])

    # --Apply Reta cut
    RetaCut = np.greater_equal(ph_Reta,trig_Reta_Cut)
    # --Apply Rhad cut
    RhadCut = np.less_equal(ph_Rhad,trig_Rhad_Cut)
    # --Apply Eratio cut
    EratioCut = np.greater_equal(ph_Eratio,trig_Eratio_Cut)

    # NOTE: element wize ops.
    # inCrack = np.abs(ph_eta) > 2.37 or (1.37 < np.abs(ph_eta) < 1.52)
    fst_incrack = np.greater(abs_ph_eta, 2.37)
    snd_incrack = np.logical_and(np.greater(abs_ph_eta,1.37),np.less(abs_ph_eta,1.52))
    inCrack =np.logical_or(fst_incrack, snd_incrack)

    # -- Set pass condition as fullfilling all cuts plus not being in a crack region
    PassCond = RetaCut & EratioCut & RhadCut

    return PassCond

