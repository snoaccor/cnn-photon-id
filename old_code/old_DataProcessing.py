#!/usr/bin/env python3
import numpy as np

class Data(object):
    """
    Data class for data pre-processing
    Training type CNN
    """

    def __init__(self,Stg=1,Dir=None):
        if Dir is not None:
            self.Dir = Dir
        else:
            # --Set default dir as Mohamed's
            self.Dir = '/sps/atlas/m/mbelfkir/';
        self.Stg = Stg;
        self.loadTestData();
        self.loadValData();
        self.loadTrainData();
        self.setXYZ();

    def setXYZ(self):
        # --Set Cluster NumpyArrays
        self.X1_Train = self.TrainDataX1[:,:];
        self.X2_Train = self.TrainDataX2[:,:];
        self.X3_Train = self.TrainDataX3[:,:];
        # --Set Labels
        self.Y_Train = self.TrainDataY[:,-1];
        # NOTE: The following strongly depends on how the file
        #       'features.txt' is defined.
        #       That might be problematic

        # --Set SSvars
        self.Z_Train = self.TrainDataY[:,:-1]

        # print(f'Ytrain: {self.Y_Train}, Ztrain: {self.Z_Train}')

        self.X1_Val = self.ValDataX1[:,:];
        self.X2_Val = self.ValDataX2[:,:];
        self.X3_Val = self.ValDataX3[:,:];

        self.Y_Val = self.ValDataY[:,-1];

        self.Z_Val = self.ValDataY[:,:-1]

        self.X1_Test = self.TestDataX1[:,:];
        self.X2_Test = self.TestDataX2[:,:];
        self.X3_Test = self.TestDataX3[:,:];

        self.Y_Test = self.TestDataY[:,-1];
        self.Z_Test = self.TestDataY[:,:-1];

        self.SS_Train = self.SetSSVar(self.Z_Train)
        self.SS_Val   = self.SetSSVar(self.Z_Val)
        self.SS_Test  = self.SetSSVar(self.Z_Test)

    # def loadGlobalData(self):
        # # -- NOTE: when is this used?
        # self.GlobalDataX = np.load(self.GetName('GammaJet','Tree_Train','X2'), allow_pickle=True);
        # self.GlobalDataY = np.load(self.GetName('GammaJet','Tree_Train','Y'), allow_pickle=True);

    def loadTrainData(self):
        print('Loading Training Data ...')

        self.TrainDataX1 = np.load(self.GetName('GammaJet','Tree_Train','X2'), allow_pickle=True);
        self.TrainDataX1 = np.append(self.TrainDataX1, np.load(self.GetName('DiJet','Tree_Train','X2'), allow_pickle=True), axis=0)
        self.TrainDataX2 = np.load(self.GetName('GammaJet','Tree_Train','X1'), allow_pickle=True);
        self.TrainDataX2 = np.append(self.TrainDataX2, np.load(self.GetName('DiJet','Tree_Train','X1'), allow_pickle=True), axis=0)
        self.TrainDataX3 = np.load(self.GetName('GammaJet','Tree_Train','X3'), allow_pickle=True);
        self.TrainDataX3 = np.append(self.TrainDataX3, np.load(self.GetName('DiJet','Tree_Train','X3'), allow_pickle=True), axis=0)
        self.TrainDataY  = np.load(self.GetName('GammaJet','Tree_Train','Y'), allow_pickle=True);
        self.TrainDataY  = np.append(self.TrainDataY, np.load(self.GetName('DiJet','Tree_Train','Y'), allow_pickle=True), axis=0)

    def loadTestData(self):
        print('Loading Test Data ...')

        self.TestDataX1 = np.load(self.GetName('GammaJet','Tree_Test','X2'), allow_pickle=True);
        self.TestDataX1 = np.append(self.TestDataX1, np.load(self.GetName('DiJet','Tree_Test','X2'), allow_pickle=True), axis=0)
        self.TestDataX2 = np.load(self.GetName('GammaJet','Tree_Test','X1'), allow_pickle=True);
        self.TestDataX2 = np.append(self.TestDataX2, np.load(self.GetName('DiJet','Tree_Test','X1'), allow_pickle=True), axis=0)
        self.TestDataX3 = np.load(self.GetName('GammaJet','Tree_Test','X3'), allow_pickle=True);
        self.TestDataX3 = np.append(self.TestDataX3, np.load(self.GetName('DiJet','Tree_Test','X3'), allow_pickle=True), axis=0)
        self.TestDataY  = np.load(self.GetName('GammaJet','Tree_Test','Y'), allow_pickle=True);
        self.TestDataY  = np.append(self.TestDataY, np.load(self.GetName('DiJet','Tree_Test','Y'), allow_pickle=True), axis=0)

    def loadValData(self):
        print('Loading Validation Data ...')

        self.ValDataX1 = np.load(self.GetName('GammaJet','Tree_Val','X2'), allow_pickle=True);
        self.ValDataX1 = np.append(self.ValDataX1, np.load(self.GetName('DiJet','Tree_Val','X2'), allow_pickle=True), axis=0)
        self.ValDataX2 = np.load(self.GetName('GammaJet','Tree_Val','X1'), allow_pickle=True);
        self.ValDataX2 = np.append(self.ValDataX2, np.load(self.GetName('DiJet','Tree_Val','X1'), allow_pickle=True), axis=0)
        self.ValDataX3 = np.load(self.GetName('GammaJet','Tree_Val','X3'), allow_pickle=True);
        self.ValDataX3 = np.append(self.ValDataX3, np.load(self.GetName('DiJet','Tree_Val','X3'), allow_pickle=True), axis=0)
        self.ValDataY = np.load(self.GetName('GammaJet','Tree_Val','Y'), allow_pickle=True);
        self.ValDataY = np.append(self.ValDataY, np.load(self.GetName('DiJet','Tree_Val','Y'), allow_pickle=True), axis=0)

    def SetSSVar(self, a):
        """
        Stores all ShowerShape values into one single numpy array.

        # Args::
            a:: numpy array to store all SS vars

        # Returns:: a
        """
        # NOTE we might want to use a PandasDataFrame?
        a1 = a[:,20:24];
        a2 = a[:,25:35];
        a3 = a[:,37:39];
        a4 = a[:,40:42];
        a5 = a[:,43:44];

        a1 = np.append(a1,a2, axis=-1)
        a1 = np.append(a1,a3, axis=-1)
        a1 = np.append(a1,a4, axis=-1)
        a1 = np.append(a1,a5, axis=-1)

        return a1

    def GetName(self, a, t, x):
        # TODO: change it to standar python string formatting.
        name = f'{self.Dir}Stg_{str(self.Stg)}/{t}/{a}_IsoTight.{t}.Stg_{str(self.Stg)}_1_{x}.npy';
        # name = self.Dir+'Stg_'+str(self.Stg)+'/'+t+'/'+a+'_IsoTight.'+t+'.Stg_'+str(self.Stg)+'_1_'+x+'.npy';
        return name;

    def AppendSamples(self,treename,var,fmt):
        # filepath = self.GetName()
        filepath_sig = self.NewGetName(treename,'GammaJet',var,fmt)
        filepath_bkg = self.NewGetName(treename,'DiJet',var,fmt)

        out_var = np.load(filepath_sig, allow_pickle=True)
        out_var = np.append(
                arr=out_var,
                values=np.load(filepath_bkg,allow_pickle=True),
                axis=0,
                )

        return out_var

    def NewGetName(self,treename,sample,var,fmt):
        """
        works with NTupleToImg output

        Args::

        """
        path = f'{self.Dir}/{self.Stg}/{treename}/'
        filename = f'{sample}_IsoTight_{var}.{fmt}'
        out = os.path.join(path,filename)
        return out

class CNNData(Data):
    """
    Load Images from npy files

    Args::

        Usage: (string), either 'OFFLINE' or 'HLT', intended use of the CNN model.
        OFFLINE uses the 3 layers of EMCalo, HLT only layer 2.

    """
    def __init__(self,Stg,Dir,Usage):
        super().__init__(Stg,Dir)
        self.usage = Usage
        if Dir is not None:
            self.data = Data(Stg,Dir)
        else:
            self.data = Data(Stg)

        print(' --> Loading CNN Data ...')
        self.Img_Lr2_Train = self.setData('Train','Lr2')[:,:,:]
        self.Img_Lr2_Val   = self.setData('Val','Lr2')[:,:,:]
        self.Img_Lr2_Test  = self.setData('Test','Lr2')[:,:,:]

        print(' --> Lr2 Image Data Loaded')

        if self.usage == 'OFFLINE':
            self.Img_Lr1_Train = self.setData('Train','Lr1')[:,:,:]
            self.Img_Lr1_Val   = self.setData('Val','Lr1')[:,:,:]
            self.Img_Lr1_Test  = self.setData('Test','Lr1')[:,:,:]

            self.Img_Lr3_Train = self.setData('Train','Lr3')[:,:,:]
            self.Img_Lr3_Val   = self.setData('Val','Lr3')[:,:,:]
            self.Img_Lr3_Test  = self.setData('Test','Lr3')[:,:,:]

            print(' --> All layers Image Data Loaded')

    def setData(self, name, lr):
        fname = f'{self.Dir}Stg_{str(self.data.Stg)}/Tree_{name}/Img_{lr}_{name}.npy'
        # fname = self.Dir+'Stg_'+str(self.data.Stg)+'/Tree_'+name+'/Img_'+lr+'_'+name+'.npy'
        # fname = self.data.Dir+'Stg_'+str(self.data.Stg)+'/Tree_'+name+'/Img_'+lr+'_'+name+'.npy'
        return np.load(fname, allow_pickle=True);


