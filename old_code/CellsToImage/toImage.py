#!/usr/bin/env python3
import numpy as np
import sys
from argparse import ArgumentParser
sys.path.append('../')
from DataProcessing.DataProcessing import Data
# sys.path.append('../../')
# from tools.DataProcessing import Data

def getArgs():
    """
    Get arguments from command line.
    """
    args = ArgumentParser(description="Arguments for Array for ANN")
    args.add_argument('-i',
                      '--inputdir',
                      required=False,
                      action='store',
                      default='/sps/atlas/m/mbelfkir',
                      help='Input Directory')
    args.add_argument('-o',
                      '--outdir',
                      required=False,
                      action='store',
                      default='/sps/atlas/m/mbelfkir/Stg_1/',
                      help='Output Directory')
    return args.parse_args()

def setData(data, Eta, Phi):
    """
    Builds the Cluster Image from the cells in data.
    Args:
        data:
        Eta: (int), total number of cells expected in the Eta axis.
        Phi: (int), total number of cells expected in the Phi axis

    Returns:
        out: (np.mat)
    """
    out = []
    for i in range(0,data.shape[0]):
        #print(i, data.shape[0])
        mtrx = []
        sumE = np.sum(data[i,:])

        for phi in range(0,Phi):
            #print('Phi : ', phi, Phi)
            EtaCl = []
            for eta in range(0,Eta):
                if sumE == 0:
                    EtaCl.append(data[i,phi + Phi*eta])
                else:
                    EtaCl.append(data[i,phi + Phi*eta]/sumE)
            mtrx.append(EtaCl)
        # mtrx2 = np.matrix(mtrx)
        mtrx2 = np.mat(mtrx)    # numpy docs sujests the use of np.mat instead of np.matrix
        out.append(mtrx2)
    return np.asarray(out)

# def Execute(data):
    # """
    # Transforms NumpyArrays to NumpyMatrix
    # for each EMC Layer and Sample type
    # """

    # Lr2_Train = setData(data.X2_Train[:,0,:], 7, 11)
    # Lr2_Val   = setData(data.X2_Val[:,0,:], 7, 11)
    # Lr2_Test  = setData(data.X2_Test[:,0,:], 7, 11)

    # Lr1_Train = setData(data.X1_Train[:,0,:], 56, 2)
    # Lr1_Val   = setData(data.X1_Val[:,0,:], 56, 2)
    # Lr1_Test  = setData(data.X1_Test[:,0,:], 56, 2)

    # Lr3_Train = setData(data.X3_Train[:,0,:], 4, 11)
    # Lr3_Val   = setData(data.X3_Val[:,0,:], 4, 11)
    # Lr3_Test  = setData(data.X3_Test[:,0,:], 4, 11)

def main(argv):
    # -- Get options from parser.
    args = getArgs()

    # -- Pre-process data located in Dir with Strategy
    # data = Data(Strategy=1,Dir=args.inputdir)
    data = Data(Stg=1,Dir=args.inputdir)

    # -- Set outdir
    # path = '/sps/atlas/m/mbelfkir/Stg_1/'
    # path = '../Array/output/Stg_1/'
    path = args.outdir


    # np.save(path+'Tree_Train/Img_Lr2_Train.npy',Lr2_Train)
    # np.save(path+'Tree_Val/Img_Lr2_Val.npy',Lr2_Val)
    # np.save(path+'Tree_Test/Img_Lr2_Test.npy',Lr2_Test)

    # np.save(path+'Tree_Train/Img_Lr1_Train.npy',Lr1_Train)
    # np.save(path+'Tree_Val/Img_Lr1_Val.npy',Lr1_Val)
    # np.save(path+'Tree_Test/Img_Lr1_Test.npy',Lr1_Test)

    # np.save(path+'Tree_Train/Img_Lr3_Train.npy',Lr3_Train)
    # np.save(path+'Tree_Val/Img_Lr3_Val.npy',Lr3_Val)
    # np.save(path+'Tree_Test/Img_Lr3_Test.npy',Lr3_Test)

    Lr2_Train = setData(data.X2_Train[:,0,:], 7, 11)
    Lr2_Val   = setData(data.X2_Val[:,0,:], 7, 11)
    Lr2_Test  = setData(data.X2_Test[:,0,:], 7, 11)

    np.save(path+'Tree_Train/Img_Lr2_Train.npy',Lr2_Train)
    np.save(path+'Tree_Val/Img_Lr2_Val.npy',Lr2_Val)
    np.save(path+'Tree_Test/Img_Lr2_Test.npy',Lr2_Test)


    Lr1_Train = setData(data.X1_Train[:,0,:], 56, 2)
    Lr1_Val   = setData(data.X1_Val[:,0,:], 56, 2)
    Lr1_Test  = setData(data.X1_Test[:,0,:], 56, 2)

    np.save(path+'Tree_Train/Img_Lr1_Train.npy',Lr1_Train)
    np.save(path+'Tree_Val/Img_Lr1_Val.npy',Lr1_Val)
    np.save(path+'Tree_Test/Img_Lr1_Test.npy',Lr1_Test)

    Lr3_Train = setData(data.X3_Train[:,0,:], 4, 11)
    Lr3_Val   = setData(data.X3_Val[:,0,:], 4, 11)
    Lr3_Test  = setData(data.X3_Test[:,0,:], 4, 11)

    np.save(path+'Tree_Train/Img_Lr3_Train.npy',Lr3_Train)
    np.save(path+'Tree_Val/Img_Lr3_Val.npy',Lr3_Val)
    np.save(path+'Tree_Test/Img_Lr3_Test.npy',Lr3_Test)

    return

if __name__ == '__main__':
    main(sys.argv[1:])
