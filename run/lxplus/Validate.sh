#!bin/sh

PATH_VAL="../../src/NeuralNetworks/Val"
CONFIGPATH="../../../run/config.ini"
PATH_WEIGHTS=""
PATH_ARCH=""

# -- cd to dir
cd $PATH_VAL &&
# This is for the default .h5 saving
./val.py -c $CONFIGPATH -Pwgt $PATH_WEIGHTS -Parch $PATH_ARCH


