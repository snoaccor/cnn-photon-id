#!bin/sh

# 'input' should contain .root files from ZllAthAnalysis
# 'samples_ex' can should have one sample type at a time 
# (it can refer to many files though)
#./NTupleToImage.py -i 'input/' -o 'output' -s 'samples_ex.txt' -f 'features_ex.txt'

GLOBAL_OUTDIR=

GLOBAL_OUTDIR=$GLOBAL_OUTDIR ./../../src/DataProcessing/NTupleToImage.py -c '../../config.yaml'
