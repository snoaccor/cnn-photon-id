#!bin/sh

PATH_TRAIN="../../src/NeuralNetworks/Train"
CONFIGPATH="../../../run/config.ini"
cd $PATH_TRAIN &&
./train.py -c $CONFIGPATH;

