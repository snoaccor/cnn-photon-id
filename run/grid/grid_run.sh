#!/bin/sh

#==================#
# shell script to test running DataProcessing/NTupleToImage
# at the grid, run it from the base dir.
#==================#

OUTDS=$1
INDS=$2

echo "your input args are: output - $OUTDS, input - $INDS"

echo " ---> setting env";
echo "python version is `python3 --version`";

python3 -m pip install pip --upgrade --user &&
    python3 -m pip install -r py_requirements.txt --user;

echo "print tree of files";
ls src/DataProcessing/;

# single file test
python3 NTupleToImage.py -i $INDS -o $OUTDS \
    -s 'src/DataProcessing/samples.txt' -f 'src/DataProcessing/features.txt'

