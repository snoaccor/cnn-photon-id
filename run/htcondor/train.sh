#!bin/sh

# As default the script to run would be 3 levels deeper than the config file.
CONFIGPATH="../../../config_lxplus.ini"

# -- Untar source code
tar -xvf "cnn_ph_id_src.tar.gz"
# -- Set env.
python3 -m pip install pip --upgrade --user &&
python3 -m pip install -r "py_requirements.txt";
echo " ** Environment set ** "

echo " --> Where am I? ";
pwd;
echo " --> travelling to script location "
cd "src/NeuralNetworks/Train" &&
echo " --> We are now here: ${PWD}";
echo " --> path to config file: ${CONFIGPATH} "

./train.py -c $CONFIGPATH
