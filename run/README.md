# How to Run

## The config file

The variables one can configure in this file are those defined in the [`parser.py`](../src/common/parser.py);
more info about the config file [here](../docs/config.md)

## Run @lxplus

1. login to your lxplus account.
2. cd `<path_to_CnnPhotonId>`
3. `make`
4. `cd run/lxplus/;`
5.     
    ```shell
    source <run_script.sh>
    ```

Each module has more details on the available ways to run.

## Run @HTCondor

1. login to your lxplus account.
2. in the `<config_file>` set:
    >   `g_outdir = <full_path_to_your_eos>`
3. targz the `src` dir if it doesn't exist or is outdated:
    >   `tar -cvzf cnn_ph_id_src.tar.gz <path_to_src>`
4. in the `cnn_photon_id.sub` set:
    1. `executable  = <script_to_exe>`
    2. where to store `output`, `error`, `log` in `/afs/`
    3. job settings:
        - job flavours:
            - espresso      = 20 min.
            - microcentury  = 1 hr.
            - longlunch     = 2 hr.
            - workday       = 8 hr.
            - tomorrow      = 1 day.
            - testmatch     = 3 days.
            - nextweek      = 7 days.
    4. make sure the following files are transfer: `<config_file>`, `<src.tar.gz>`, `<requirements>`
5. 
    ```shell
    condor_submit cnn_photon_id.sub
    ```
More info [here](https://batchdocs.web.cern.ch/tutorial/exercise6b.html) and [here](https://htcondor.readthedocs.io/en/latest/users-manual/submitting-a-job.html#submitting-jobs-using-a-shared-file-system).

If the input and output data is stored in `\eos\` then you don't need to transfer those files,
those folders will be available in the batch system.

## Run @Grid

**TODO**
